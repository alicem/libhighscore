Title: Core Descriptors
Slug: core-descriptors

# Core Descriptors

For Highscore to register the core, it must come with a core descriptor. A core
descriptor is a file in the [struct@GLib.KeyFile] format, containing information
about the core.

The files must have the `.highscore` extension and are installed into the
following directory: `$LIBDIR/highscore/cores`.

An example descriptor is as follows:

```ini
[Highscore]
Name=Example
Version=1.2.3
Commit=123456abc-dirty
Module=example-highscore.so
Authors=Jane Doe;
Copyright=Copyright (C) 2023 John Doe;Copyright (C) 2024 Jane Doe;
Website=https://example.org/
License=GPL-2.0-or-later
Platforms=NintendoEntertainmentSystem;FamicomDiskSystem;
StateVersion=1
```

Each descriptor must have a `[Highscore]` group, containing the following fields:

- `Name` - human readable name of the core;
- `Version` - upstream version;
- `Module` - relative path to the core library file;
- `Authors` - list of core authors, separated with semicolons;
- `Copyright` - list of copyright strings, separated with semicolons;
- `License` - core license, in the form of an SPDX identifier;
- `Platforms` - list of supported game platforms.

Optionally, it can have the following keys:

- `Commit` - git commit/svn revision/etc. If left empty, treated same as omitted.
    Can include `-dirty` to indicate that it's dirty;
- `StateVersion` - indicates savestate compatibility and should be incremented
    whenever it breaks. If not present, the version is assumed to be 0.

## Platforms

The supported platforms are as follows:

- `Atari2600` - [enum@Hs.Platform.ATARI_2600];
- `Atari7800` - [enum@Hs.Platform.ATARI_7800];
- `AtariLynx` - [enum@Hs.Platform.ATARI_LYNX];
- `FamicomDiskSystem` - [enum@Hs.Platform.FAMICOM_DISK_SYSTEM];
- `GameBoy` - [enum@Hs.Platform.GAME_BOY];
- `GameBoyAdvance` - [enum@Hs.Platform.GAME_BOY_ADVANCE];
- `GameGear` - [enum@Hs.Platform.GAME_GEAR];
- `MasterSystem` - [enum@Hs.Platform.MASTER_SYSTEM];
- `MegaDrive` - [enum@Hs.Platform.MEGA_DRIVE];
- `NeoGeoPocket` - [enum@Hs.Platform.NEO_GEO_POCKET];
- `NintendoEntertainmentSystem` - [enum@Hs.Platform.NES];
- `Nintendo64` - [enum@Hs.Platform.NINTENDO_64];
- `NintendoDS` - [enum@Hs.Platform.NINTENDO_DS];
- `PcEngine` - [enum@Hs.Platform.PC_ENGINE];
- `PcEngineCD` - [enum@Hs.Platform.PC_ENGINE_CD];
- `PlayStation` - [enum@Hs.Platform.PLAYSTATION];
- `SegaSaturn` - [enum@Hs.Platform.SEGA_SATURN];
- `SG1000` - [enum@Hs.Platform.SG1000];
- `SuperNintendoEntertainmentSystem` - [enum@Hs.Platform.SUPER_NES];
- `VirtualBoy` - [enum@Hs.Platform.VIRTUAL_BOY];
- `WonderSwan` - [enum@Hs.Platform.WONDERSWAN].
