option('introspection', type: 'feature', value: 'auto')
option('vapi', type: 'boolean', value: true)

option('gtk_doc',
       type: 'boolean', value: false,
       description: 'Whether to generate the API reference for libhighscore')