/*
 * Copyright (C) 2024 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-core-error.h"

#include <gio/gio.h>

/**
 * HsCoreError:
 * @HS_CORE_ERROR_COULDNT_LOAD_ROM: Failed to load the ROM, probably invalid ROM
 * @HS_CORE_ERROR_UNSUPPORTED_GAME: The core doesn't support this game
 * @HS_CORE_ERROR_OUT_OF_MEMORY: Out of memory
 * @HS_CORE_ERROR_MISSING_BIOS: The core requires firmware and it's missing
 * @HS_CORE_ERROR_IO: Input/output errors
 * @HS_CORE_ERROR_INTERNAL: Internal errors, e.g. core bugs
 *
 * An error code used in various methods in [class@Core].
 *
 * These errors can be used over D-Bus, mapped as:
 *
 * Error Code                            | D-Bus Error Name
 * ------------------------------------- | ------------------------------------------------------------
 * [error@Hs.CoreError.COULDNT_LOAD_ROM] | `org.gnome.gitlab.alicem.libhighscore.Error.CouldntLoadROM`
 * [error@Hs.CoreError.UNSUPPORTED_GAME] | `org.gnome.gitlab.alicem.libhighscore.Error.UnsupportedGame`
 * [error@Hs.CoreError.OUT_OF_MEMORY]    | `org.gnome.gitlab.alicem.libhighscore.Error.OutOfMemory`
 * [error@Hs.CoreError.MISSING_BIOS]     | `org.gnome.gitlab.alicem.libhighscore.Error.MissingBIOS`
 * [error@Hs.CoreError.IO]               | `org.gnome.gitlab.alicem.libhighscore.Error.IO`
 * [error@Hs.CoreError.INTERNAL]         | `org.gnome.gitlab.alicem.libhighscore.Error.Internal`
 */

static const GDBusErrorEntry hs_core_error_entries[] =
{
  { HS_CORE_ERROR_COULDNT_LOAD_ROM, "org.gnome.gitlab.alicem.libhighscore.Error.CouldntLoadROM" },
  { HS_CORE_ERROR_UNSUPPORTED_GAME, "org.gnome.gitlab.alicem.libhighscore.Error.UnsupportedGame" },
  { HS_CORE_ERROR_OUT_OF_MEMORY,    "org.gnome.gitlab.alicem.libhighscore.Error.OutOfMemory" },
  { HS_CORE_ERROR_MISSING_BIOS,     "org.gnome.gitlab.alicem.libhighscore.Error.MissingBIOS" },
  { HS_CORE_ERROR_IO,               "org.gnome.gitlab.alicem.libhighscore.Error.IO" },
  { HS_CORE_ERROR_INTERNAL,         "org.gnome.gitlab.alicem.libhighscore.Error.Internal" }
};

G_STATIC_ASSERT (G_N_ELEMENTS (hs_core_error_entries) == HS_CORE_N_ERRORS);

/**
 * hs_core_error_quark:
 *
 * Gets the `HsCore` error quark.
 *
 * Returns: the quark
 */
GQuark
hs_core_error_quark (void)
{
  static gsize quark = 0;
  g_dbus_error_register_error_domain ("hs-core-error-quark",
                                      &quark,
                                      hs_core_error_entries,
                                      G_N_ELEMENTS (hs_core_error_entries));
  return (GQuark) quark;
}
