/*
 * Copyright (C) 2024 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib.h>

G_BEGIN_DECLS

#define HS_CORE_ERROR (hs_core_error_quark ())

GQuark hs_core_error_quark (void);

typedef enum {
  HS_CORE_ERROR_COULDNT_LOAD_ROM,
  HS_CORE_ERROR_UNSUPPORTED_GAME,
  HS_CORE_ERROR_OUT_OF_MEMORY,
  HS_CORE_ERROR_MISSING_BIOS,
  HS_CORE_ERROR_IO,
  HS_CORE_ERROR_INTERNAL,
  HS_CORE_N_ERRORS /*< skip >*/
} HsCoreError;

G_END_DECLS
