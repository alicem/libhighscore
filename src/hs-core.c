/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-core.h"

#include "hs-core-error.h"

/**
 * HsCore:
 *
 * Base class for the cores.
 *
 * To register the core in the frontend, it must have an exported function
 * `GType hs_get_core_type (void)`, returning the [alias@GObject.Type] of the
 * core class.
 *
 * Subclasses must override the following virtual methods:
 *
 * - [vfunc@Core.load_rom]
 * - [vfunc@Core.poll_input]
 * - [vfunc@Core.run_frame]
 * - [vfunc@Core.reset]
 * - [vfunc@Core.reload_save]
 * - [vfunc@Core.load_state]
 * - [vfunc@Core.save_state]
 * - [vfunc@Core.get_frame_rate]
 * - [vfunc@Core.get_aspect_ratio]
 * - [vfunc@Core.get_sample_rate]
 *
 * Everything else is optional and should be overridden as needed for the
 * particular core.
 *
 * `HsCore` also provides methods proxying to [iface@Frontend], such as
 * [method@Core.play_samples]. This is done just for convenience, so that cores
 * don't have to use [method@Core.get_frontend] manually.
 *
 * ## Platform Support
 *
 * Every platform has its own interface for platform-specific methods:
 * [iface@NesCore] for NES and so on. Cores must implement the corresponding
 * interfaces for their supported platforms.
 *
 * ## Running the Game
 *
 * Every core must override the [vfunc@Core.load_rom] method and load the
 * provided game, as well as initialize input and output within it.
 *
 * After that, the game can be started using the [vfunc@Core.start] method, and
 * stopped with [vfunc@Core.stop]. When pausing and unpausing the game,
 * [vfunc@Core.pause] and [vfunc@Core.resume] will be called respectively. All
 * of these methods are optional, for example a single-threaded core doesn't
 * need to override `pause()` and `resume()`.
 *
 * On every frame, the frontend calls [vfunc@Core.run_frame]. The core is
 * expected to run a single frame of emulation in it, as well as output video
 * and audio as appropriate.
 *
 * The game can be reset with [vfunc@Core.reset].
 *
 * ## Region
 *
 * Cores should override the [vfunc@Core.get_region] method to signal their
 * region (NTSC/PAL) where appropriate. The frontend may use this info for e.g.
 * deciding which video filter to use.
 *
 * ## Input
 *
 * The core must override the [vfunc@Core.poll_input] method and read input from
 * the provided [struct@InputState].
 *
 * `HsInputState` contains input data for every platform, and the core must only
 * access the state from its current platform. If the core supports multiple
 * platforms, it must query the current platform first. Accessing any other
 * platform will result in undefined behavior.
 *
 * The specific layout of the input data will differ depending on the platform,
 * see the corresponding documentation - for example, [struct@NesInputState] for
 * NES.
 *
 * ::: note
 *     Add-on platforms do not have their own input state and reuse the one from
 *     their base platform.
 *
 * ### Rumble
 *
 * To use gamepad rumble, call [method@Core.rumble].
 *
 * ## Audio Output
 *
 * Cores must override the [vfunc@Core.get_sample_rate] method, providing their
 * audio sample rate. Optionally, [vfunc@Core.get_channels] can be overridden as
 * well, providing the number of audio channels the core uses. If not
 * overridden, frontend will assume 2 channels.
 *
 * To play audio, the core should call [method@Core.play_samples]. The number
 * of samples should be consistent with the framerate.
 *
 * ## Video Output
 *
 * Every core must override the [vfunc@Core.get_frame_rate] and
 * [vfunc@Core.get_aspect_ratio] methods, reporting their current framerate and
 * aspect ratio.
 *
 * ### Software Rendering
 *
 * To use software rendering, the core must create a context using
 * [method@Core.create_software_context] within `load_rom()`.
 *
 * For software contexts, the size and format of the framebuffer must be
 * provided right away and cannot be changed later, though the core is allowed
 * to recreate the context later. If the core can use multiple resolutions and
 * it's not known at the creation time, provide the maximum size, and then use a
 * smaller area via [method@SoftwareContext.set_area] and/or
 * [method@SoftwareContext.set_row_stride].
 *
 * To output the data, call [method@SoftwareContext.get_framebuffer] and write
 * your pixel data into that buffer. This must be done in
 * [vfunc@Core.run_frame].
 *
 * ### OpenGL Rendering
 *
 * To use OpenGL, the core must create a context using
 * [method@Core.create_gl_context] within `load_rom()`.
 *
 * GL contexts need to be realized before being used, using
 * [method@GLContext.realize]. This can fail, for example if the core requested
 * an unavailable profile or version. If that happens, the core should try a
 * different configuration or software rendering if possible, before failing to
 * load entirely.
 *
 * After that, the context needs to be resized using [method@GLContext.set_size].
 * This can be done at any time.
 *
 * The core must render its output into the default framebuffer, provided by the
 * frontend and accessed with [method@GLContext.get_default_framebuffer]. The
 * framebuffer will have a color attachment, as well as depth and stencil
 * attachment according to the flags passed into `create_gl_context()`.
 *
 * Use [method@GLContext.get_proc_address] to retrieve OpenGL symbols if the
 * core doesn't have a way to do it on its own.
 *
 * After rendering each frame, the core must call
 * [method@GLContext.swap_buffers].
 *
 * OpenGL core output will often be vertically flipped. Pass
 * [flags@Hs.GLFlags.FLIPPED] flag when creating the context to let the frontend
 * know that this is the case.
 *
 * In some cases, the output data will be pre-downloaded and post-processed on
 * CPU. Instead of uploading it back to GPU, pass the
 * [flags@Hs.GLFlags.DIRECT_FB_ACCESS] flag when creating the context, and then
 * use [method@GLContext.acquire_framebuffer] and
 * [method@GLContext.release_framebuffer] to access the underlying framebuffer.
 *
 * ::: important
 *     Using direct framebuffer access prevents some optimizations in the
 *     frontend and should only be used as the last resort.
 *
 * Before disposing the context, [method@GLContext.unrealize] must be called.
 *
 * ## Save Data
 *
 * The save data is stored in the save location, received as the `save_path`
 * parameter in [vfunc@Core.load_rom] and [vfunc@Core.reload_save].
 *
 * The core can either save its data directly into that location, or create a
 * folder there and have save data inside it.
 *
 * The frontend provides no guarantees about the ROM filenames, so the core must
 * not base its save data filenames on the ROM filename. Instead, use names like
 * `save`, `save.dat`, or base it on the game's internal identifiers.
 *
 * The core does not have to monitor the save location for changes - when the
 * frontend changes the save data, it must call [vfunc@Core.reload_save] to
 * request that. Note that the save path may be different with every
 * `reload_save()` call, and the core needs to switch to the new path and forget
 * about the previous location.
 *
 * Some cores update their save data as soon as possible, whenever the game
 * modifies it. Those cores that don't do that must implement
 * [vfunc@Core.sync_save] and save it there.
 *
 * ## Save States
 *
 * Cores must override two methods: [vfunc@Core.load_state] and
 * [vfunc@Core.save_state]. Both of these methods are asynchronous and don't
 * have to be completed right away.
 *
 * ## Multiple Media
 *
 * Some games come on multiple media, e.g. multiple CDs on PlayStation. In that
 * case, [vfunc@Core.load_rom] will receive multiple filenames in `rom_paths`
 * and the core will need to switch which disc is currently active.
 *
 * For this, the core must implement [vfunc@Core.get_current_media] and
 * [vfunc@Core.set_current_media]. They will be called by the frontend to query
 * and change the current media respectively.
 *
 * When changing the current media from the core (both within
 * `set_current_media()` and otherwise), the core must call
 * [method@Core.notify_current_media].
 *
 * The frontend can access [property@Core:current-media] for convenience.
 *
 * ## Logging
 *
 * To log information, use [method@Core.log]. It's `printf()`-formatted and has
 * [method@Core.log_valist] and [method@Core.log_literal] variants as well.
 *
 * Cores should not output anything directly to standard output.
 *
 * ## Cache
 *
 * Some cores need a location for temporary files, shader cache etc. Use
 * [method@Core.get_cache_path] to get the cache location. Cores should not use
 * any other directories for this purpose.
 *
 * The core is responsible for creating the cache file/directory if it doesn't
 * exist.
 *
 * ::: note
 *     The frontend may use a shared directory for everything or a separate
 *     directory for each game. Do not store per-game data like saves in the
 *     cache location.
 */

typedef struct
{
  char *name;
  HsPlatform platform;
  HsFrontend *frontend;
  gboolean running;
  gboolean paused;

  gboolean saving_state;
  gboolean loading_state;
  GTask *save_load_state_task;
} HsCorePrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (HsCore, hs_core, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_NAME,
  PROP_PLATFORM,
  PROP_FRONTEND,
  PROP_CURRENT_MEDIA,
  N_PROPS
};

static GParamSpec *properties[N_PROPS];

static void save_load_state_done (HsCore  *self,
                                  GError **error);

static void
save_load_state_cancelled_cb (GCancellable *cancellable,
                              HsCore       *self)
{
  GError *error;

  g_set_error (&error, G_IO_ERROR, G_IO_ERROR_CANCELLED, "Operation was cancelled");

  save_load_state_done (self, &error);
}

static void
save_load_state_done (HsCore  *self,
                      GError **error)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);
  GCancellable *cancellable = g_task_get_cancellable (priv->save_load_state_task);

  if (cancellable)
    g_signal_handlers_disconnect_by_func (cancellable, save_load_state_cancelled_cb, self);

  if (error) {
    g_task_return_error (priv->save_load_state_task, *error);
    return;
  }

  g_task_return_int (priv->save_load_state_task, TRUE);
}

static void
hs_core_real_start (HsCore *self)
{
}

static void
hs_core_real_stop (HsCore *self)
{
}

static void
hs_core_real_pause (HsCore *self)
{
}

static void
hs_core_real_resume (HsCore *self)
{
}

static gboolean
hs_core_real_sync_save (HsCore  *self,
                        GError **error)
{
  return TRUE;
}

static int
hs_core_real_get_channels (HsCore *self)
{
  return 2;
}

static HsRegion
hs_core_real_get_region (HsCore *self)
{
  return HS_REGION_UNKNOWN;
}

static guint
hs_core_real_get_current_media (HsCore *self)
{
  return 0;
}

static void
hs_core_real_set_current_media (HsCore *self,
                                guint   current_media)
{
}

static void
hs_core_get_property (GObject    *object,
                      guint       prop_id,
                      GValue     *value,
                      GParamSpec *pspec)
{
  HsCore *self = HS_CORE (object);

  switch (prop_id) {
  case PROP_NAME:
    g_value_set_string (value, hs_core_get_name (self));
    break;
  case PROP_PLATFORM:
    g_value_set_enum (value, hs_core_get_platform (self));
    break;
  case PROP_FRONTEND:
    g_value_set_object (value, hs_core_get_frontend (self));
    break;
  case PROP_CURRENT_MEDIA:
    g_value_set_uint (value, hs_core_get_current_media (self));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
hs_core_set_property (GObject      *object,
                      guint         prop_id,
                      const GValue *value,
                      GParamSpec   *pspec)
{
  HsCore *self = HS_CORE (object);
  HsCorePrivate *priv = hs_core_get_instance_private (self);

  switch (prop_id) {
  case PROP_NAME:
    g_set_str (&priv->name, g_value_get_string (value));
    break;
  case PROP_PLATFORM:
    priv->platform = g_value_get_enum (value);
    break;
  case PROP_FRONTEND:
    hs_core_set_frontend (self, g_value_get_object (value));
    break;
  case PROP_CURRENT_MEDIA:
    hs_core_set_current_media (self, g_value_get_uint (value));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
hs_core_dispose (GObject *object)
{
  HsCore *self = HS_CORE (object);
  HsCorePrivate *priv = hs_core_get_instance_private (self);

  g_clear_weak_pointer (&priv->frontend);

  G_OBJECT_CLASS (hs_core_parent_class)->dispose (object);
}

static void
hs_core_finalize (GObject *object)
{
  HsCore *self = HS_CORE (object);
  HsCorePrivate *priv = hs_core_get_instance_private (self);

  g_free (priv->name);

  G_OBJECT_CLASS (hs_core_parent_class)->finalize (object);
}

static void
hs_core_class_init (HsCoreClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = hs_core_get_property;
  object_class->set_property = hs_core_set_property;
  object_class->dispose = hs_core_dispose;
  object_class->finalize = hs_core_finalize;

  klass->start = hs_core_real_start;
  klass->stop = hs_core_real_stop;
  klass->pause = hs_core_real_pause;
  klass->resume = hs_core_real_resume;
  klass->sync_save = hs_core_real_sync_save;
  klass->get_channels = hs_core_real_get_channels;
  klass->get_region = hs_core_real_get_region;
  klass->get_current_media = hs_core_real_get_current_media;
  klass->set_current_media = hs_core_real_set_current_media;

  /**
   * HsCore:name:
   *
   * The core name, set by the frontend.
   *
   * Matches the `Name` key from the descriptor file.
   */
  properties[PROP_NAME] =
    g_param_spec_string ("name", NULL, NULL,
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  /**
   * HsCore:platform:
   *
   * The game platform, set by the frontend.
   *
   * Cores that support multiple platforms can query it to determine the current
   * platform.
   */
  properties[PROP_PLATFORM] =
    g_param_spec_enum ("platform", NULL, NULL,
                       HS_TYPE_PLATFORM,
                       HS_PLATFORM_UNKNOWN,
                       G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  /**
   * HsCore:frontend:
   *
   * The `HsFrontend` instance, set by the frontend.
   *
   * Cores shouldn't touch this property directly, but instead use methods like
   * [method@Core.play_samples].
   */
  properties[PROP_FRONTEND] =
    g_param_spec_object ("frontend", NULL, NULL,
                         HS_TYPE_FRONTEND,
                         G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY);

  /**
   * HsCore:current-media:
   *
   * Index of the current media (e.g. a CD on PlayStation).
   *
   * To use it, cores must implement [vfunc@Core.get_current_media] and
   * [vfunc@Core.set_current_media]. Otherwise, it's always set to 0.
   *
   * To notify this property, use [method@Core.notify_current_media].
   */
  properties[PROP_CURRENT_MEDIA] =
    g_param_spec_uint ("current-media", NULL, NULL,
                       0, G_MAXUINT, 0,
                       G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
hs_core_init (HsCore *self)
{
}

/**
 * hs_core_get_name:
 * @self: a core
 *
 * Gets the name of @self.
 *
 * Matches the `Name` key from the descriptor file.
 */
const char *
hs_core_get_name (HsCore *self)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);

  g_return_val_if_fail (HS_IS_CORE (self), NULL);

  return priv->name;
}

/**
 * hs_core_get_platform:
 * @self: a core
 *
 * Gets the current game platform.
 *
 * Cores that support multiple platforms can query it to determine the current
 * platform.
 */
HsPlatform
hs_core_get_platform (HsCore *self)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);

  g_return_val_if_fail (HS_IS_CORE (self), HS_PLATFORM_UNKNOWN);

  return priv->platform;
}

/**
 * hs_core_get_frontend:
 * @self: a core
 *
 * Gets the `HsFrontend` instance.
 *
 * Cores shouldn't call this method directly, but instead use methods like
 * [method@Core.play_samples].
 *
 * Returns: (transfer none) (nullable): the frontend instance
 */
HsFrontend *
hs_core_get_frontend (HsCore *self)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);

  g_return_val_if_fail (HS_IS_CORE (self), NULL);

  return priv->frontend;
}

/**
 * hs_core_set_frontend:
 * @self: a core
 * @frontend: (transfer none) (nullable): the new frontend instance
 *
 * Sets the `HsFrontend` instance.
 *
 * This should be called from the frontend to enable methods like
 * [method@Core.play_samples] to work.
 *
 * Cores must never call this function.
 */
void
hs_core_set_frontend (HsCore     *self,
                      HsFrontend *frontend)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);

  g_return_if_fail (HS_IS_CORE (self));
  g_return_if_fail (frontend == NULL || HS_IS_FRONTEND (frontend));
  g_return_if_fail (!priv->running);

  if (!g_set_weak_pointer (&priv->frontend, frontend))
    return;

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_FRONTEND]);
}

/**
 * hs_core_load_rom:
 * @self: a core
 * @rom_paths: (array length=n_rom_paths): an array of file paths
 * @n_rom_paths: length of @rom_paths
 * @save_path: path of the save location
 * @error: return location for the error
 *
 * Loads a game from @rom_paths, with the save data at @save_path.
 *
 * If the save data exists, it's guaranteed to be present at @save_path by the
 * time this method is called.
 *
 * The core should initialize the emulation, as well as audio and video output
 * here.
 *
 * Returns: `TRUE` on successful load, `FALSE` on error
 */
gboolean
hs_core_load_rom (HsCore      *self,
                  const char **rom_paths,
                  int          n_rom_paths,
                  const char  *save_path,
                  GError     **error)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);
  gboolean ret;

  g_return_val_if_fail (HS_IS_CORE (self), FALSE);
  g_return_val_if_fail (rom_paths != NULL, FALSE);
  g_return_val_if_fail (n_rom_paths > 0, FALSE);
  g_return_val_if_fail (save_path != NULL, FALSE);
  g_return_val_if_fail (!priv->running, FALSE);

  if (priv->frontend == NULL)
    g_error ("HsCore must have its frontend set before loading ROM");

  g_assert (HS_CORE_GET_CLASS (self)->load_rom);

  ret = HS_CORE_GET_CLASS (self)->load_rom (self,
                                            rom_paths,
                                            n_rom_paths,
                                            save_path,
                                            error);

  priv->running = !!ret;

  return ret;
}

/**
 * hs_core_start:
 * @self: a core
 *
 * Starts the game.
 *
 * The game must have already been loaded in the [vfunc@Core.load_rom] function.
 */
void
hs_core_start (HsCore *self)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);

  g_return_if_fail (HS_IS_CORE (self));
  g_return_if_fail (priv->running);

  g_assert (HS_CORE_GET_CLASS (self)->start);

  HS_CORE_GET_CLASS (self)->start (self);
}

/**
 * hs_core_poll_input:
 * @self: a core
 * @input_state: a snapshot of the current input state
 *
 * Reads input from @input_state.
 *
 * Frontend will call this before [vfunc@Core.run_frame]. Cores must read the
 * provided input from @input_state here, corresponding to their current
 * platform.
 *
 * ::: note
 *     Add-on platforms do not have their own input state and reuse the one from
 *     their base platform.
 */
void
hs_core_poll_input (HsCore       *self,
                    HsInputState *input_state)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);

  g_return_if_fail (HS_IS_CORE (self));
  g_return_if_fail (input_state != NULL);
  g_return_if_fail (priv->running);

  g_assert (HS_CORE_GET_CLASS (self)->poll_input);

  HS_CORE_GET_CLASS (self)->poll_input (self, input_state);
}

/**
 * hs_core_run_frame:
 * @self: a core
 *
 * Runs a single frame of emulation.
 *
 * When using software rendering, the cores are expected to access
 * [method@SoftwareContext.get_framebuffer] here and nowhere else.
 */
void
hs_core_run_frame (HsCore *self)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);

  g_return_if_fail (HS_IS_CORE (self));
  g_return_if_fail (priv->running);

  g_assert (HS_CORE_GET_CLASS (self)->run_frame);

  HS_CORE_GET_CLASS (self)->run_frame (self);
}

/**
 * hs_core_reset:
 * @self: a core
 * @hard: whether to perform a hard reset
 *
 * Resets the game running in @self.
 *
 * If @hard is `TRUE`, performs a hard reset (turning the system off and back
 * on), otherwise a soft reset if available (pressing the reset button).
 */
void
hs_core_reset (HsCore   *self,
               gboolean  hard)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);

  g_return_if_fail (HS_IS_CORE (self));
  g_return_if_fail (priv->running);

  hard = !!hard;

  g_assert (HS_CORE_GET_CLASS (self)->reset);

  HS_CORE_GET_CLASS (self)->reset (self, hard);
}

/**
 * hs_core_stop:
 * @self: a core
 *
 * Stops the running the game.
 *
 * The game must have already been loaded in the [vfunc@Core.load_rom] function
 * and started with [vfunc@Core.start].
 *
 * The core should undo everything created in `load_rom()`. When using GL
 * rendering, [method@GLContext.unrealize] should be called before disposing
 * the context.
 */
void
hs_core_stop (HsCore *self)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);

  g_return_if_fail (HS_IS_CORE (self));
  g_return_if_fail (priv->running);

  g_assert (HS_CORE_GET_CLASS (self)->stop);

  HS_CORE_GET_CLASS (self)->stop (self);

  priv->running = FALSE;
}

/**
 * hs_core_pause:
 * @self: a core
 *
 * Called when @self is paused.
 *
 * Multi-threaded cores should pause the background threads here.
 */
void
hs_core_pause (HsCore *self)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);

  g_return_if_fail (HS_IS_CORE (self));
  g_return_if_fail (priv->running);
  g_return_if_fail (!priv->paused);

  g_assert (HS_CORE_GET_CLASS (self)->pause);

  HS_CORE_GET_CLASS (self)->pause (self);

  priv->paused = TRUE;
}

/**
 * hs_core_resume:
 * @self: a core
 *
 * Called when @self is resumed from a pause.
 *
 * Multi-threaded cores should unpause the background threads here.
 */
void
hs_core_resume (HsCore *self)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);

  g_return_if_fail (HS_IS_CORE (self));
  g_return_if_fail (priv->running);
  g_return_if_fail (priv->paused);

  g_assert (HS_CORE_GET_CLASS (self)->resume);

  HS_CORE_GET_CLASS (self)->resume (self);

  priv->paused = FALSE;
}

/**
 * hs_core_sync_save:
 * @self: a core
 * @error: return location for the error
 *
 * Syncs the save data.
 *
 * Cores that don't sync it as soon as the game modifies it must do it here
 * instead.
 *
 * Returns: `TRUE` on success, `FALSE` if there was an error.
 */
gboolean
hs_core_sync_save (HsCore  *self,
                   GError **error)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);

  g_return_val_if_fail (HS_IS_CORE (self), FALSE);
  g_return_val_if_fail (priv->running, FALSE);

  g_assert (HS_CORE_GET_CLASS (self)->sync_save);

  return HS_CORE_GET_CLASS (self)->sync_save (self, error);
}

/**
 * hs_core_reload_save:
 * @self: a core
 * @save_path: path of the save location
 * @error: return location for the error
 *
 * Reloads the save data from @save_path.
 *
 * The core must switch its save data location to @save_path and forget about
 * the previous location.
 *
 * If the save data exists, it's guaranteed to be present at @save_path by the
 * time this method is called.
 *
 * Returns: `TRUE` on success, `FALSE` if there was an error.
 */
gboolean
hs_core_reload_save (HsCore      *self,
                     const char  *save_path,
                     GError     **error)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);

  g_return_val_if_fail (HS_IS_CORE (self), FALSE);
  g_return_val_if_fail (save_path != NULL, FALSE);
  g_return_val_if_fail (priv->running, FALSE);

  g_assert (HS_CORE_GET_CLASS (self)->reload_save);

  return HS_CORE_GET_CLASS (self)->reload_save (self, save_path, error);
}

/**
 * hs_core_load_state:
 * @self: a core
 * @path: path to the save state file
 * @cancellable: an optional cancellable object
 * @callback: (scope async) (closure user_data): the async callback to call when
 *     the load is finished
 * @user_data: the data to pass to callback function
 *
 * Loads a save state from @path.
 */
void
hs_core_load_state (HsCore              *self,
                    const char          *path,
                    GCancellable        *cancellable,
                    GAsyncReadyCallback  callback,
                    gpointer             user_data)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);

  g_return_if_fail (HS_IS_CORE (self));
  g_return_if_fail (path != NULL);
  g_return_if_fail (priv->running);
  g_return_if_fail (!priv->saving_state);
  g_return_if_fail (!priv->loading_state);

  g_assert (HS_CORE_GET_CLASS (self)->load_state);

  priv->save_load_state_task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (priv->save_load_state_task, hs_core_load_state);
  g_task_set_task_data (priv->save_load_state_task, g_strdup (path), (GDestroyNotify) g_free);

  if (cancellable)
    g_signal_connect (cancellable, "cancelled", G_CALLBACK (save_load_state_cancelled_cb), self);

  priv->loading_state = TRUE;

  HS_CORE_GET_CLASS (self)->load_state (self, path, save_load_state_done);
}

/**
 * hs_core_load_state_finish:
 * @self: a core
 * @result: a `GAsyncResult`
 * @error: return location for the error
 *
 * Finishes an asynchronous save state loading operation started with
 * [method@Core.load_state].
 *
 * Returns: `TRUE` on successful load, `FALSE` on error
 */
gboolean
hs_core_load_state_finish (HsCore        *self,
                           GAsyncResult  *result,
                           GError       **error)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);
  gboolean ret;

  g_return_val_if_fail (HS_IS_CORE (self), FALSE);
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (g_task_get_source_tag (G_TASK (result)) == hs_core_load_state, FALSE);
  g_return_val_if_fail (priv->running, FALSE);

  ret = g_task_propagate_int (G_TASK (result), error);

  priv->loading_state = FALSE;
  g_clear_object (&priv->save_load_state_task);

  return ret;
}

/**
 * hs_core_save_state:
 * @self: a core
 * @path: path to the save state location
 * @cancellable: an optional cancellable object
 * @callback: (scope async) (closure user_data): the async callback to call when
 *     the save is finished
 * @user_data: the data to pass to callback function
 *
 * Saves the current state into @path.
 */
void
hs_core_save_state (HsCore              *self,
                    const char          *path,
                    GCancellable        *cancellable,
                    GAsyncReadyCallback  callback,
                    gpointer             user_data)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);

  g_return_if_fail (HS_IS_CORE (self));
  g_return_if_fail (path != NULL);
  g_return_if_fail (priv->running);
  g_return_if_fail (!priv->saving_state);
  g_return_if_fail (!priv->loading_state);

  g_assert (HS_CORE_GET_CLASS (self)->save_state);

  priv->save_load_state_task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (priv->save_load_state_task, hs_core_save_state);
  g_task_set_task_data (priv->save_load_state_task, g_strdup (path), (GDestroyNotify) g_free);

  if (cancellable)
    g_signal_connect (cancellable, "cancelled", G_CALLBACK (save_load_state_cancelled_cb), self);

  priv->saving_state = TRUE;

  HS_CORE_GET_CLASS (self)->save_state (self, path, save_load_state_done);
}

/**
 * hs_core_save_state_finish:
 * @self: a core
 * @result: a `GAsyncResult`
 * @error: return location for the error
 *
 * Finishes an asynchronous state saving operation started with
 * [method@Core.save_state].
 *
 * Returns: `TRUE` on successful load, `FALSE` on error
 */
gboolean
hs_core_save_state_finish (HsCore        *self,
                           GAsyncResult  *result,
                           GError       **error)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);
  gboolean ret;

  g_return_val_if_fail (HS_IS_CORE (self), FALSE);
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (g_task_get_source_tag (G_TASK (result)) == hs_core_save_state, FALSE);
  g_return_val_if_fail (priv->running, FALSE);

  ret = g_task_propagate_int (G_TASK (result), error);

  priv->saving_state = FALSE;
  g_clear_object (&priv->save_load_state_task);

  return ret;
}

/**
 * hs_core_get_frame_rate:
 * @self: a core
 *
 * Gets the frame rate of @self in frames per second.
 *
 * Returns: the frame rate
 */
double
hs_core_get_frame_rate (HsCore *self)
{
  g_return_val_if_fail (HS_IS_CORE (self), 0.0);

  g_assert (HS_CORE_GET_CLASS (self)->get_frame_rate);

  return HS_CORE_GET_CLASS (self)->get_frame_rate (self);
}

/**
 * hs_core_get_aspect_ratio:
 * @self: a core
 *
 * Gets the aspect ratio of @self.
 *
 * Returns: the aspect ratio
 */
double
hs_core_get_aspect_ratio (HsCore *self)
{
  g_return_val_if_fail (HS_IS_CORE (self), 0.0);

  g_assert (HS_CORE_GET_CLASS (self)->get_aspect_ratio);

  return HS_CORE_GET_CLASS (self)->get_aspect_ratio (self);
}

/**
 * hs_core_get_sample_rate:
 * @self: a core
 *
 * Gets the sample rate of @self.
 *
 * Returns: the sample rate
 */
double
hs_core_get_sample_rate (HsCore *self)
{
  g_return_val_if_fail (HS_IS_CORE (self), 0.0);

  g_assert (HS_CORE_GET_CLASS (self)->get_sample_rate);

  return HS_CORE_GET_CLASS (self)->get_sample_rate (self);
}

/**
 * hs_core_get_channels:
 * @self: a core
 *
 * Gets the number of audio channels @self uses.
 *
 * If not implemented, assumes 2 channel.
 *
 * Returns: the number of channels.
 */
int
hs_core_get_channels (HsCore *self)
{
  g_return_val_if_fail (HS_IS_CORE (self), 0);

  g_assert (HS_CORE_GET_CLASS (self)->get_channels);

  return HS_CORE_GET_CLASS (self)->get_channels (self);
}

/**
 * hs_core_get_region:
 * @self: a core
 *
 * Gets the region of the current game.
 *
 * If not implemented, assumes `[enum@Hs.Region.UNKNOWN].
 *
 * Returns: the region
 */
HsRegion
hs_core_get_region (HsCore *self)
{
  g_return_val_if_fail (HS_IS_CORE (self), HS_REGION_UNKNOWN);

  g_assert (HS_CORE_GET_CLASS (self)->get_region);

  return HS_CORE_GET_CLASS (self)->get_region (self);
}

/**
 * hs_core_play_samples:
 * @self: a core
 * @samples: (array length=n_samples): the sample data
 * @n_samples: length of @samples
 *
 * Plays provided audio samples.
 *
 * The samples must be 16 bit, signed, interleaved.
 */
void
hs_core_play_samples (HsCore *self,
                      gint16 *samples,
                      int     n_samples)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);

  g_return_if_fail (HS_IS_CORE (self));
  g_return_if_fail (samples != NULL);
  g_return_if_fail (n_samples > 0);
  g_return_if_fail (priv->frontend != NULL);

  hs_frontend_play_samples (priv->frontend, samples, n_samples);
}

/**
 * hs_core_rumble:
 * @self: a core
 * @player: the player to do rumble for
 * @strong_magnitude: the magnitude for the heavy motor
 * @weak_magnitude: the magnitude for the light motor
 * @milliseconds: the rumble effect play time in milliseconds
 *
 * Makes @player's controller rumble for @milliseconds.
 *
 * The heavy and light motors will rumble at their respectively defined
 * magnitudes, in the [0-1] range, 0 meaning no rumble, 1 meaning very strong
 * rumble.
 *
 * @milliseconds cannot exceed [const@MAX_RUMBLE_DURATION].
 */
void
hs_core_rumble (HsCore  *self,
                guint    player,
                double   strong_magnitude,
                double   weak_magnitude,
                guint16  milliseconds)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);

  g_return_if_fail (HS_IS_CORE (self));
  g_return_if_fail (priv->frontend != NULL);
  g_return_if_fail (milliseconds <= HS_MAX_RUMBLE_DURATION);

  hs_frontend_rumble (priv->frontend, player, strong_magnitude, weak_magnitude, milliseconds);
}

/**
 * hs_core_create_software_context:
 * @self: a core
 * @width: framebuffer width
 * @height: framebuffer height
 * @format: the pixel format
 *
 * Creates a software rendering context for @self.
 *
 * The context will have a framebuffer with the size @width × @height, with the
 * pixel format defined by @format. The size and format cannot be changed later,
 * though the core is allowed to recreate the context.
 *
 * If the core can use multiple resolutions and it's not known at the creation
 * time, provide the maximum size, and then use a smaller area via
 * [method@SoftwareContext.set_area] and/or
 * [method@SoftwareContext.set_row_stride].
 *
 * Returns: (transfer full): a newly created software rendering context
 */
HsSoftwareContext *
hs_core_create_software_context (HsCore        *self,
                                 guint          width,
                                 guint          height,
                                 HsPixelFormat  format)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);

  g_return_val_if_fail (HS_IS_CORE (self), NULL);
  g_return_val_if_fail (format <= HS_PIXEL_FORMAT_B8G8R8X8, NULL);
  g_return_val_if_fail (priv->frontend != NULL, NULL);

  return hs_frontend_create_software_context (priv->frontend, width, height, format);
}

/**
 * hs_core_create_gl_context:
 * @self: a core
 * @profile: the OpenGL profile
 * @major_version: major version, e.g. 3 in 3.2
 * @minor_version: minor version, e.g. 2 in 3.2
 * @flags: additional parameters for the context
 *
 * Creates an OpenGL context for @self.
 *
 * The context will use the provided profile and version.
 *
 * The context must be realized before use, using [method@GLContext.realize].
 * `realize()` can fail, for example if the requested profile and/or version is
 * not available.
 *
 * Returns: (transfer full): a newly created OpenGL context
 */
HsGLContext *
hs_core_create_gl_context (HsCore      *self,
                           HsGLProfile  profile,
                           int          major_version,
                           int          minor_version,
                           HsGLFlags    flags)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);

  g_return_val_if_fail (HS_IS_CORE (self), NULL);
  g_return_val_if_fail (profile <= HS_GL_PROFILE_ES, NULL);
  g_return_val_if_fail (priv->frontend != NULL, NULL);

  return hs_frontend_create_gl_context (priv->frontend, profile,
                                        major_version, minor_version, flags);
}

/**
 * hs_core_get_cache_path:
 * @self: a core
 *
 * Gets the path to the cache location.
 *
 * It can be used for temporary files, cache and so on. Cores should not use
 * any other directories for this purpose.

 * The core is responsible for creating the cache file/directory if it doesn't
 * exist.
 *
 * ::: note
 *     The frontend may use a shared directory for everything or a separate
 *     directory for each game. Do not store per-game data like saves in the
 *     cache location.
 *
 * Returns: (transfer full): the cache path
 */
char *
hs_core_get_cache_path (HsCore *self)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);

  g_return_val_if_fail (HS_IS_CORE (self), NULL);
  g_return_val_if_fail (priv->frontend != NULL, NULL);

  return hs_frontend_get_cache_path (priv->frontend);
}

/**
 * hs_core_log:
 * @self: a core
 * @level: the log level
 * @format: printf()-style format for the message
 * @...: parameters for message format
 *
 * Logs a message formatted with @format with the log level @level.
 *
 * Cores should not output anything directly to stdout.
 */
void
hs_core_log (HsCore     *self,
             HsLogLevel  level,
             const char *format,
             ...)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);
  g_autofree char *message = NULL;
  va_list args;

  g_return_if_fail (HS_IS_CORE (self));
  g_return_if_fail (level >= HS_LOG_DEBUG);
  g_return_if_fail (level <= HS_LOG_CRITICAL);
  g_return_if_fail (format != NULL);
  g_return_if_fail (priv->frontend != NULL);

  va_start (args, format);
  message = g_strdup_vprintf (format, args);
  va_end (args);

  hs_frontend_log (priv->frontend, level, message);
}

/**
 * hs_core_log_valist:
 * @self: a core
 * @level: the log level
 * @format: `printf()`-style format for the message
 * @args: list of parameters for the message format
 *
 * Logs a message formatted with @format with the log level @level.
 *
 * Cores should not output anything directly to stdout.
 */
void
hs_core_log_valist (HsCore     *self,
                    HsLogLevel  level,
                    const char *format,
                    va_list     args)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);
  g_autofree char *message = NULL;

  g_return_if_fail (HS_IS_CORE (self));
  g_return_if_fail (level >= HS_LOG_DEBUG);
  g_return_if_fail (level <= HS_LOG_CRITICAL);
  g_return_if_fail (format != NULL);
  g_return_if_fail (priv->frontend != NULL);

  message = g_strdup_vprintf (format, args);

  hs_frontend_log (priv->frontend, level, message);
}

/**
 * hs_core_log_literal:
 * @self: a core
 * @level: the log level
 * @message: the message to log
 *
 * Logs @message with the log level @level.
 *
 * Cores should not output anything directly to stdout.
 */
void
hs_core_log_literal (HsCore     *self,
                     HsLogLevel  level,
                     const char *message)
{
  HsCorePrivate *priv = hs_core_get_instance_private (self);

  g_return_if_fail (HS_IS_CORE (self));
  g_return_if_fail (level >= HS_LOG_DEBUG);
  g_return_if_fail (level <= HS_LOG_CRITICAL);
  g_return_if_fail (message != NULL);
  g_return_if_fail (priv->frontend != NULL);

  hs_frontend_log (priv->frontend, level, message);
}

/**
 * hs_core_get_current_media:
 * @self: a core
 *
 * Gets index of the current media (e.g. a CD on PlayStation).
 *
 * Returns: index of the current media
 */
guint
hs_core_get_current_media (HsCore *self)
{
  g_return_val_if_fail (HS_IS_CORE (self), 0);

  g_assert (HS_CORE_GET_CLASS (self)->get_current_media);

  return HS_CORE_GET_CLASS (self)->get_current_media (self);
}

/**
 * hs_core_set_current_media:
 * @self: a core
 * @media: index of the newly selected media
 *
 * Sets index of the current media (e.g. a CD on PlayStation).
 */
void
hs_core_set_current_media (HsCore *self,
                           guint   media)
{
  g_return_if_fail (HS_IS_CORE (self));

  g_assert (HS_CORE_GET_CLASS (self)->set_current_media);

  HS_CORE_GET_CLASS (self)->set_current_media (self, media);
}

/**
 * hs_core_notify_current_media:
 * @self: a core
 *
 * Called when the current media index changes.
 *
 * Notifies [property@Core:current-media].
 */
void
hs_core_notify_current_media (HsCore *self)
{
  g_return_if_fail (HS_IS_CORE (self));

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_CURRENT_MEDIA]);
}
