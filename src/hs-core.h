/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-frontend.h"
#include "hs-platform.h"
#include "hs-region.h"

#include <gio/gio.h>

G_BEGIN_DECLS

typedef union _HsInputState HsInputState;

#define HS_TYPE_CORE (hs_core_get_type())

G_DECLARE_DERIVABLE_TYPE (HsCore, hs_core, HS, CORE, GObject)

/**
 * HsStateCallback:
 * @self: a core instance
 * @error: (nullable) (transfer full): an error to report
 *
 * The callback to call after finishing [vfunc@Core.load_state] or
 * [vfunc@Core.save_state].
 *
 * Pass `NULL` to @error to indicate success, otherwise pass the error.
 */
typedef void (* HsStateCallback) (HsCore  *self,
                                  GError **error);

struct _HsCoreClass
{
  GObjectClass parent_class;

  gboolean (* load_rom) (HsCore      *self,
                         const char **rom_paths,
                         int          n_rom_paths,
                         const char  *save_path,
                         GError     **error);

  void (* start) (HsCore *self);

  void (* poll_input) (HsCore       *self,
                       HsInputState *input_state);

  void (* run_frame) (HsCore *self);

  void (* reset) (HsCore   *self,
                  gboolean  hard);

  void (* stop) (HsCore *self);

  void (* pause) (HsCore *self);

  void (* resume) (HsCore *self);

  gboolean (* reload_save) (HsCore      *self,
                            const char  *save_path,
                            GError     **error);

  gboolean (* sync_save) (HsCore  *self,
                          GError **error);

  /**
   * HsCoreClass.load_state:
   * @path: path to the save state file
   * @callback: a callback to call after finishing the load operation
   *
   * Loads a save state from @path.
   *
   * The core must call @callback once it's finished, passing the error it got
   * if the load failed, or `NULL` if it succeeded.
   */
  void (* load_state) (HsCore          *self,
                       const char      *path,
                       HsStateCallback  callback);
  /**
   * HsCoreClass.save_state:
   * @path: path to the save state location
   * @callback: a callback to call after finishing the save operation
   *
   * Saves the current state into @path.
   *
   * The core must call @callback once it's finished, passing the error it got
   * if the load failed, or `NULL` if it succeeded.
   */
  void (* save_state) (HsCore          *self,
                       const char      *path,
                       HsStateCallback  callback);

  double (* get_frame_rate) (HsCore *self);

  double (* get_aspect_ratio) (HsCore *self);

  double (* get_sample_rate) (HsCore *self);

  int (* get_channels) (HsCore *self);

  HsRegion (* get_region) (HsCore *self);

  guint (* get_current_media) (HsCore *self);
  void  (* set_current_media) (HsCore *self,
                               guint   media);
};

const char *hs_core_get_name (HsCore *self);

HsPlatform hs_core_get_platform (HsCore *self);

HsFrontend *hs_core_get_frontend (HsCore *self);
void        hs_core_set_frontend (HsCore     *self,
                                  HsFrontend *frontend);

gboolean hs_core_load_rom (HsCore      *self,
                           const char **rom_paths,
                           int          n_rom_paths,
                           const char  *save_path,
                           GError     **error);

void hs_core_start (HsCore *self);

void hs_core_poll_input (HsCore       *self,
                         HsInputState *input_state);

void hs_core_run_frame (HsCore *self);

void hs_core_reset (HsCore   *self,
                    gboolean  hard);

void hs_core_stop (HsCore *self);

void hs_core_pause (HsCore *self);

void hs_core_resume (HsCore *self);

gboolean hs_core_reload_save (HsCore      *self,
                              const char  *save_path,
                              GError     **error);

gboolean hs_core_sync_save (HsCore  *self,
                            GError **error);

void     hs_core_load_state        (HsCore              *self,
                                    const char          *path,
                                    GCancellable        *cancellable,
                                    GAsyncReadyCallback  callback,
                                    gpointer             user_data);
gboolean hs_core_load_state_finish (HsCore              *self,
                                    GAsyncResult        *result,
                                    GError             **error);

void     hs_core_save_state        (HsCore              *self,
                                    const char          *path,
                                    GCancellable        *cancellable,
                                    GAsyncReadyCallback  callback,
                                    gpointer             user_data);
gboolean hs_core_save_state_finish (HsCore              *self,
                                    GAsyncResult        *result,
                                    GError             **error);

double hs_core_get_frame_rate (HsCore *self);

double hs_core_get_aspect_ratio (HsCore *self);

double hs_core_get_sample_rate (HsCore *self);

int hs_core_get_channels (HsCore *self);

HsRegion hs_core_get_region (HsCore *self);

void hs_core_play_samples (HsCore *self,
                           gint16 *samples,
                           int     n_samples);

void hs_core_rumble (HsCore  *self,
                     guint    player,
                     double   strong_magnitude,
                     double   weak_magnitude,
                     guint16  milliseconds);

HsSoftwareContext *hs_core_create_software_context (HsCore        *self,
                                                    guint          width,
                                                    guint          height,
                                                    HsPixelFormat  format);

HsGLContext *hs_core_create_gl_context (HsCore      *self,
                                        HsGLProfile  profile,
                                        int          major_version,
                                        int          minor_version,
                                        HsGLFlags    flags);

char *hs_core_get_cache_path (HsCore *self);

void hs_core_log         (HsCore     *self,
                          HsLogLevel  level,
                          const char *format,
                          ...) G_GNUC_PRINTF (3, 4);
void hs_core_log_valist  (HsCore     *self,
                          HsLogLevel  level,
                          const char *format,
                          va_list     args) G_GNUC_PRINTF (3, 0);
void hs_core_log_literal (HsCore     *self,
                          HsLogLevel  level,
                          const char *message);

guint hs_core_get_current_media    (HsCore *self);
void  hs_core_set_current_media    (HsCore *self,
                                    guint   media);
void  hs_core_notify_current_media (HsCore *self);

G_END_DECLS
