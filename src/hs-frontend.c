/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-frontend.h"

G_DEFINE_INTERFACE (HsFrontend, hs_frontend, G_TYPE_OBJECT)

/**
 * HsLogLevel:
 * @HS_LOG_DEBUG: Log level for debug messages.
 * @HS_LOG_INFO: Log level for informational messages.
 * @HS_LOG_MESSAGE: Log level for messages.
 * @HS_LOG_WARNING: Log level for warnings.
 * @HS_LOG_CRITICAL: Log level for critical warning messages.
 *
 * Describes log levels for [method@Core.log].
 */

/**
 * HsFrontend:
 *
 * An interface defining a frontend.
 *
 * The frontend must have a `HsFrontend` implementation and set it as the
 * [property@Core:frontend] value.
 */

static void
hs_frontend_default_init (HsFrontendInterface *iface)
{
}

/**
 * hs_frontend_play_samples:
 * @self: a frontend
 * @samples: (array length=n_samples): the sample data
 * @n_samples: length of @samples
 *
 * Plays provided audio samples.
 *
 * The samples must be 16 bit, signed, interleaved.
 */
void
hs_frontend_play_samples (HsFrontend *self,
                          gint16     *samples,
                          int         n_samples)
{
  HsFrontendInterface *iface;

  g_return_if_fail (HS_IS_FRONTEND (self));

  iface = HS_FRONTEND_GET_IFACE (self);

  g_assert (iface->play_samples);

  iface->play_samples (self, samples, n_samples);
}

/**
 * hs_frontend_rumble:
 * @self: a frontend
 * @player: the player to do rumble for
 * @strong_magnitude: the magnitude for the heavy motor
 * @weak_magnitude: the magnitude for the light motor
 * @milliseconds: the rumble effect play time in milliseconds
 *
 * Makes @player's controller rumble for @milliseconds.
 *
 * The heavy and light motors will rumble at their respectively defined
 * magnitudes, in the [0-1] range, 0 meaning no rumble, 1 meaning very strong
 * rumble.
 *
 * @milliseconds cannot exceed [const@MAX_RUMBLE_DURATION].
 */
void
hs_frontend_rumble (HsFrontend *self,
                    guint       player,
                    double      strong_magnitude,
                    double      weak_magnitude,
                    guint16     milliseconds)
{
  HsFrontendInterface *iface;

  g_return_if_fail (HS_IS_FRONTEND (self));
  g_return_if_fail (milliseconds <= HS_MAX_RUMBLE_DURATION);

  strong_magnitude = CLAMP (strong_magnitude, 0, 1);
  weak_magnitude = CLAMP (weak_magnitude, 0, 1);

  iface = HS_FRONTEND_GET_IFACE (self);

  g_assert (iface->rumble);

  iface->rumble (self, player, strong_magnitude, weak_magnitude, milliseconds);
}

/**
 * hs_frontend_create_software_context:
 * @self: a frontend
 * @width: framebuffer width
 * @height: framebuffer height
 * @format: the pixel format
 *
 * Creates a software rendering context.
 *
 * The context will have a framebuffer with the size @width × @height, with the
 * pixel format defined by @format. The size and format cannot be changed later,
 * though the core is allowed to recreate the context.
 *
 * If the core can use multiple resolutions and it's not known at the creation
 * time, provide the maximum size, and then use a smaller area via
 * [method@SoftwareContext.set_area] and/or
 * [method@SoftwareContext.set_row_stride].
 *
 * Returns: (transfer full): a newly created software rendering context
 */
HsSoftwareContext *
hs_frontend_create_software_context (HsFrontend    *self,
                                     guint          width,
                                     guint          height,
                                     HsPixelFormat  format)
{
  HsFrontendInterface *iface;

  g_return_val_if_fail (HS_IS_FRONTEND (self), NULL);
  g_return_val_if_fail (format <= HS_PIXEL_FORMAT_B8G8R8X8, NULL);

  iface = HS_FRONTEND_GET_IFACE (self);

  g_assert (iface->create_software_context);

  return iface->create_software_context (self, width, height, format);
}

/**
 * hs_frontend_create_gl_context:
 * @self: a frontend
 * @profile: the OpenGL profile
 * @major_version: major version, e.g. 3 in 3.2
 * @minor_version: minor version, e.g. 2 in 3.2
 * @flags: additional parameters for the context
 *
 * Creates an OpenGL context.
 *
 * The context will use the provided profile and version.
 *
 * The context must be realized before use, using [method@GLContext.realize].
 * `realize()` can fail, for example if the requested profile and/or version is
 * not available.
 *
 * Returns: (transfer full): a newly created OpenGL context
 */
HsGLContext *
hs_frontend_create_gl_context (HsFrontend  *self,
                               HsGLProfile  profile,
                               int          major_version,
                               int          minor_version,
                               HsGLFlags    flags)
{
  HsFrontendInterface *iface;

  g_return_val_if_fail (HS_IS_FRONTEND (self), NULL);
  g_return_val_if_fail (profile <= HS_GL_PROFILE_ES, NULL);

  iface = HS_FRONTEND_GET_IFACE (self);

  g_assert (iface->create_gl_context);

  return iface->create_gl_context (self, profile, major_version, minor_version, flags);
}

/**
 * hs_frontend_get_cache_path:
 * @self: a frontend
 *
 * Gets the path to the cache location.
 *
 * It can be used for temporary files, cache and so on. Cores should not use
 * any other directories for this purpose.

 * The core is responsible for creating the cache file/directory if it doesn't
 * exist.
 *
 * The frontend is allowed to use a shared directory for everything or a
 * separate directory for each game.
 *
 * Returns: (transfer full): the cache path
 */
char *
hs_frontend_get_cache_path (HsFrontend *self)
{
  HsFrontendInterface *iface;

  g_return_val_if_fail (HS_IS_FRONTEND (self), NULL);

  iface = HS_FRONTEND_GET_IFACE (self);

  g_assert (iface->get_cache_path);

  return iface->get_cache_path (self);
}

/**
 * hs_frontend_log:
 * @self: a frontend
 * @level: the log level
 * @message: the message to log
 *
 * Logs @message with the log level @level.
 *
 * Cores should not output anything directly to stdout.
 */
void
hs_frontend_log (HsFrontend *self,
                 HsLogLevel  level,
                 const char *message)
{
  HsFrontendInterface *iface;

  g_return_if_fail (HS_IS_FRONTEND (self));
  g_return_if_fail (level >= HS_LOG_DEBUG);
  g_return_if_fail (level <= HS_LOG_CRITICAL);
  g_return_if_fail (message != NULL);

  iface = HS_FRONTEND_GET_IFACE (self);

  g_assert (iface->log);

  return iface->log (self, level, message);
}
