/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-gl-context.h"
#include "hs-pixel-format.h"
#include "hs-software-context.h"

G_BEGIN_DECLS

/**
 * HS_MAX_RUMBLE_DURATION:
 *
 * The maximum allowed duration in [method@Core.rumble], in milliseconds.
 */
#define HS_MAX_RUMBLE_DURATION 32767

#define HS_TYPE_FRONTEND (hs_frontend_get_type ())

G_DECLARE_INTERFACE (HsFrontend, hs_frontend, HS, FRONTEND, GObject)

typedef enum {
  HS_LOG_DEBUG,
  HS_LOG_INFO,
  HS_LOG_MESSAGE,
  HS_LOG_WARNING,
  HS_LOG_CRITICAL,
} HsLogLevel;

struct _HsFrontendInterface
{
  GTypeInterface parent;

  void (* play_samples) (HsFrontend *self,
                         gint16     *samples,
                         int         n_samples);

  void (* rumble) (HsFrontend *self,
                   guint       player,
                   double      strong_magnitude,
                   double      weak_magnitude,
                   guint16     milliseconds);

  HsSoftwareContext * (* create_software_context) (HsFrontend    *self,
                                                   guint          width,
                                                   guint          height,
                                                   HsPixelFormat  format);

  HsGLContext * (* create_gl_context) (HsFrontend  *self,
                                       HsGLProfile  profile,
                                       int          major_version,
                                       int          minor_version,
                                       HsGLFlags    flags);

  char * (* get_cache_path) (HsFrontend *self);

  void (* log) (HsFrontend *self,
                HsLogLevel  level,
                const char *message);
};

void hs_frontend_play_samples (HsFrontend *self,
                               gint16     *samples,
                               int         n_samples);

void hs_frontend_rumble (HsFrontend *self,
                         guint       player,
                         double      strong_magnitude,
                         double      weak_magnitude,
                         guint16     milliseconds);

HsSoftwareContext *hs_frontend_create_software_context (HsFrontend    *self,
                                                        guint          width,
                                                        guint          height,
                                                        HsPixelFormat  format);

HsGLContext *hs_frontend_create_gl_context (HsFrontend  *self,
                                            HsGLProfile  profile,
                                            int          major_version,
                                            int          minor_version,
                                            HsGLFlags    flags);

char *hs_frontend_get_cache_path (HsFrontend *self);

void hs_frontend_log (HsFrontend *self,
                      HsLogLevel  level,
                      const char *message);

G_END_DECLS
