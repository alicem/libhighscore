/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-gl-context.h"

G_DEFINE_INTERFACE (HsGLContext, hs_gl_context, G_TYPE_OBJECT)

/**
 * HsGLContextError:
 * @HS_GL_CONTEXT_ERROR_INCOMPATIBLE_VERSION: incompatible GL version
 *
 * An error code used in [vfunc@GLContext.realize].
 */

/**
 * HsGLProfile:
 * @HS_GL_PROFILE_CORE: Use the core OpenGL profile
 * @HS_GL_PROFILE_LEGACY: Use the compatibility OpenGL profile
 * @HS_GL_PROFILE_ES: Use OpenGL ES
 *
 * Describes OpenGL profiles for [iface@GLContext].
 */

/**
 * HsGLFlags:
 * @HS_GL_FLAGS_DEFAULT: Default flags.
 * @HS_GL_FLAGS_DEPTH: Attach a depth buffer to the default framebuffer.
 * @HS_GL_FLAGS_STENCIL: Attach a stencil buffer to the default framebuffer.
 * @HS_GL_FLAGS_FLIPPED: The output will be flipped and frontend must flip it
 *     back before displaying.
 * @HS_GL_FLAGS_DIRECT_FB_ACCESS: The core will download the data from GPU and
 *     needs direct access to a software buffer.
 * ::: important
 *     Direct framebuffer access prevents some optimizations in the frontend and
 *     should only be used as the last resort.
 *
 * Flags that can be passed when creating a [iface@GLContext].
 */

/**
 * hs_gl_context_error_quark:
 *
 * Gets the GL context error quark.
 *
 * Returns: a quark
 */
G_DEFINE_QUARK (hs-gl-context-error-quark, hs_gl_context_error)

/**
 * HsGLContext:
 *
 * An interface defining an OpenGL context.
 *
 * Cores should use [method@Core.create_gl_context] within `load_rom()` to
 * create a context.
 *
 * Frontends must implement [vfunc@Frontend.create_gl_context] and create a
 * context within it.
 *
 * `HsGLContext` needs to be realized before being used, using
 * [method@GLContext.realize]. This can fail, for example if the core requested
 * an unavailable profile or version. If that happens, the core should try a
 * different configuration or software rendering if possible, before failing to
 * load entirely.
 *
 * After that, the context needs to be resized using [method@GLContext.set_size].
 * This can be done at any time.
 *
 * The core must render its output into the default framebuffer, provided by the
 * frontend and accessed with [method@GLContext.get_default_framebuffer]. The
 * framebuffer will have a color attachment, as well as depth and stencil
 * attachment according to the flags passed into `create_gl_context()`.
 *
 * Use [method@GLContext.get_proc_address] to retrieve OpenGL symbols if the
 * core doesn't have a way to do it on its own.
 *
 * After rendering each frame, the core must call
 * [method@GLContext.swap_buffers].
 *
 * OpenGL core output will often be vertically flipped. Pass
 * [flags@Hs.GLFlags.FLIPPED] flag when creating the context to let the frontend
 * know that this is the case.
 *
 * In some cases, the output data will be pre-downloaded and post-processed on
 * CPU. Instead of uploading it back to GPU, pass the
 * [flags@Hs.GLFlags.DIRECT_FB_ACCESS] flag when creating the context, and then
 * use [method@GLContext.acquire_framebuffer] and
 * [method@GLContext.release_framebuffer] to access the underlying framebuffer.
 *
 * ::: important
 *     Using direct framebuffer access prevents some optimizations in the
 *     frontend and should only be used as the last resort.
 */

static void
hs_gl_context_default_init (HsGLContextInterface *iface)
{
}

/**
 * hs_gl_context_realize:
 * @self: a GL context
 * @error: return location for the error
 *
 * Initializes GL resources for @self.
 *
 * [error@Hs.GLContextError.INCOMPATIBLE_VERSION] will be returned if the core
 * requested an unavailable version. If this happens, the core should dispose
 * @self and try to create a context for a lower version, or otherwise fall back
 * to software rendering or fail entirely.
 *
 * Frontend is responsible for automatically unrealizing the context if an error
 * happens here.
 *
 * Returns: `TRUE` is @self was successfully realized, `FALSE` on error
 */
gboolean
hs_gl_context_realize (HsGLContext  *self,
                       GError      **error)
{
  HsGLContextInterface *iface;

  g_return_val_if_fail (HS_IS_GL_CONTEXT (self), FALSE);

  iface = HS_GL_CONTEXT_GET_IFACE (self);

  g_assert (iface->realize);

  return iface->realize (self, error);
}

/**
 * hs_gl_context_unrealize:
 * @self: a GL context
 *
 * Destroys GL resources for @self.
 *
 * Must be called by the core before disposing the context.
 */
void
hs_gl_context_unrealize (HsGLContext *self)
{
  HsGLContextInterface *iface;

  g_return_if_fail (HS_IS_GL_CONTEXT (self));

  iface = HS_GL_CONTEXT_GET_IFACE (self);

  g_assert (iface->unrealize);

  iface->unrealize (self);
}

/**
 * hs_gl_context_set_size:
 * @self: a GL context
 * @width: the framebuffer width
 * @height: the framebuffer height
 *
 * Resizes the default framebuffer to @width × @height.
 */
void
hs_gl_context_set_size (HsGLContext *self,
                        guint        width,
                        guint        height)
{
  HsGLContextInterface *iface;

  g_return_if_fail (HS_IS_GL_CONTEXT (self));

  iface = HS_GL_CONTEXT_GET_IFACE (self);

  g_assert (iface->set_size);

  iface->set_size (self, width, height);
}

/**
 * hs_gl_context_get_default_framebuffer:
 * @self: a GL context
 *
 * Gets the name of the default framebuffer object.
 *
 * The cores must render into this framebuffer, unless
 * [flags@Hs.GLFlags.DIRECT_FB_ACCESS] is set.
 */
guint
hs_gl_context_get_default_framebuffer (HsGLContext *self)
{
  HsGLContextInterface *iface;

  g_return_val_if_fail (HS_IS_GL_CONTEXT (self), 0);

  iface = HS_GL_CONTEXT_GET_IFACE (self);

  g_assert (iface->get_default_framebuffer);

  return iface->get_default_framebuffer (self);
}

/**
 * hs_gl_context_get_proc_address:
 * @self: a GL context
 * @name: name of a GL symbol
 *
 * Retrieves a GL symbol with the name @name.
 *
 * Returns: (transfer none) (nullable): location of the symbol
 */
gpointer
hs_gl_context_get_proc_address (HsGLContext *self,
                                const char  *name)
{
  HsGLContextInterface *iface;

  g_return_val_if_fail (HS_IS_GL_CONTEXT (self), NULL);

  iface = HS_GL_CONTEXT_GET_IFACE (self);

  g_assert (iface->get_proc_address);

  return iface->get_proc_address (self, name);
}

/**
 * hs_gl_context_acquire_framebuffer:
 * @self: a GL context
 *
 * Locks and returns the software framebuffer for direct rendering.
 *
 * Can only be called if [flags@Hs.GLFlags.DIRECT_FB_ACCESS] is set.
 *
 * Returns: (transfer none): location of the software framebuffer
 */
gpointer
hs_gl_context_acquire_framebuffer (HsGLContext *self)
{
  HsGLContextInterface *iface;

  g_return_val_if_fail (HS_IS_GL_CONTEXT (self), NULL);

  iface = HS_GL_CONTEXT_GET_IFACE (self);

  g_assert (iface->acquire_framebuffer);

  return iface->acquire_framebuffer (self);
}

/**
 * hs_gl_context_release_framebuffer:
 * @self: a core
 *
 * Unlocks the software framebuffer.
 *
 * Can only be called if [flags@Hs.GLFlags.DIRECT_FB_ACCESS] is set.
 */
void
hs_gl_context_release_framebuffer (HsGLContext *self)
{
  HsGLContextInterface *iface;

  g_return_if_fail (HS_IS_GL_CONTEXT (self));

  iface = HS_GL_CONTEXT_GET_IFACE (self);

  g_assert (iface->release_framebuffer);

  return iface->release_framebuffer (self);
}

/**
 * hs_gl_context_swap_buffers:
 * @self: a core
 *
 * Swap buffers and end the current frame.
 *
 * Cores must call this at the end of each frame.
 */
void
hs_gl_context_swap_buffers (HsGLContext *self)
{
  HsGLContextInterface *iface;

  g_return_if_fail (HS_IS_GL_CONTEXT (self));

  iface = HS_GL_CONTEXT_GET_IFACE (self);

  g_assert (iface->swap_buffers);

  iface->swap_buffers (self);
}
