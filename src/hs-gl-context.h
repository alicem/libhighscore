/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

G_BEGIN_DECLS

#define HS_GL_CONTEXT_ERROR (hs_gl_context_error_quark ())

GQuark hs_gl_context_error_quark (void);

typedef enum {
  HS_GL_CONTEXT_ERROR_INCOMPATIBLE_VERSION, /*< nick=incompatible-version >*/
} HsGLContextError;

typedef enum {
  HS_GL_PROFILE_CORE,
  HS_GL_PROFILE_LEGACY,
  HS_GL_PROFILE_ES,
} HsGLProfile;

typedef enum {
  HS_GL_FLAGS_DEFAULT          = 0,
  HS_GL_FLAGS_DEPTH            = 1 << 0,
  HS_GL_FLAGS_STENCIL          = 1 << 1,
  HS_GL_FLAGS_FLIPPED          = 1 << 2,
  HS_GL_FLAGS_DIRECT_FB_ACCESS = 1 << 3,
} HsGLFlags;

#define HS_TYPE_GL_CONTEXT (hs_gl_context_get_type ())

G_DECLARE_INTERFACE (HsGLContext, hs_gl_context, HS, GL_CONTEXT, GObject)

struct _HsGLContextInterface
{
  GTypeInterface parent;

  gboolean (*realize) (HsGLContext  *self,
                       GError      **error);

  void (*unrealize) (HsGLContext *self);

  void (*set_size) (HsGLContext *self,
                    guint        width,
                    guint        height);

  guint (*get_default_framebuffer) (HsGLContext *self);

  gpointer (*get_proc_address) (HsGLContext *self,
                                const char  *name);

  gpointer (* acquire_framebuffer) (HsGLContext *self);
  void     (* release_framebuffer) (HsGLContext *self);

  void (*swap_buffers) (HsGLContext *self);
};

gboolean hs_gl_context_realize (HsGLContext  *self,
                                GError      **error);

void hs_gl_context_unrealize (HsGLContext *self);

void hs_gl_context_set_size (HsGLContext *self,
                             guint        width,
                             guint        height);

guint hs_gl_context_get_default_framebuffer (HsGLContext *self);

gpointer hs_gl_context_get_proc_address (HsGLContext *self,
                                         const char  *name);

gpointer hs_gl_context_acquire_framebuffer (HsGLContext *self);
void     hs_gl_context_release_framebuffer (HsGLContext *self);

void hs_gl_context_swap_buffers (HsGLContext *self);

G_END_DECLS
