/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include "hs-atari-2600-core.h"
#include "hs-atari-7800-core.h"
#include "hs-atari-lynx-core.h"
#include "hs-game-boy-core.h"
#include "hs-game-boy-advance-core.h"
#include "hs-game-gear-core.h"
#include "hs-master-system-core.h"
#include "hs-mega-drive-core.h"
#include "hs-neo-geo-pocket-core.h"
#include "hs-nes-core.h"
#include "hs-nintendo-64-core.h"
#include "hs-nintendo-ds-core.h"
#include "hs-pc-engine-core.h"
#include "hs-pc-engine-cd-core.h"
#include "hs-playstation-core.h"
#include "hs-sega-saturn-core.h"
#include "hs-sg1000-core.h"
#include "hs-super-nes-core.h"
#include "hs-virtual-boy-core.h"
#include "hs-wonderswan-core.h"

G_BEGIN_DECLS

/**
 * HsInputState:
 * @atari_2600: Input state for [enum@Hs.Platform.ATARI_2600].
 * @atari_7800: Input state for [enum@Hs.Platform.ATARI_7800].
 * @atari_lynx: Input state for [enum@Hs.Platform.ATARI_LYNX].
 * @game_boy: Input state for [enum@Hs.Platform.GAME_BOY].
 * @game_boy_advance: Input state for [enum@Hs.Platform.GAME_BOY_ADVANCE].
 * @game_gear: Input state for [enum@Hs.Platform.GAME_GEAR].
 * @master_system: Input state for [enum@Hs.Platform.MASTER_SYSTEM].
 * @mega_drive: Input state for [enum@Hs.Platform.MEGA_DRIVE].
 * @neo_geo_pocket: Input state for [enum@Hs.Platform.NEO_GEO_POCKET].
 * @nes: Input state for [enum@Hs.Platform.NES].
 * @nintendo_64: Input state for [enum@Hs.Platform.NINTENDO_64].
 * @nintendo_ds: Input state for [enum@Hs.Platform.NINTENDO_DS].
 * @pc_engine: Input state for [enum@Hs.Platform.PC_ENGINE].
 * @psx: Input state for [enum@Hs.Platform.PLAYSTATION].
 * @saturn: Input state for [enum@Hs.Platform.SEGA_SATURN].
 * @sg1000: Input state for [enum@Hs.Platform.SG1000].
 * @super_nes: Input state for [enum@Hs.Platform.SUPER_NES].
 * @virtual_boy: Input state for [enum@Hs.Platform.VIRTUAL_BOY].
 * @wonderswan: Input state for [enum@Hs.Platform.WONDERSWAN].
 *
 * A union of input states from the different platforms.
 *
 * Cores should access it from [vfunc@Core.poll_input], only for their current
 * platform.
 * *
 * ::: note
 *     Add-on platforms do not have their own input state and reuse the one from
 *     their base platform.
 */
union _HsInputState {
  HsAtari2600InputState atari_2600;
  HsAtari7800InputState atari_7800;
  HsAtariLynxInputState atari_lynx;
  HsGameBoyInputState game_boy;
  HsGameBoyAdvanceInputState game_boy_advance;
  HsGameGearInputState game_gear;
  HsMasterSystemInputState master_system;
  HsMegaDriveInputState mega_drive;
  HsNeoGeoPocketInputState neo_geo_pocket;
  HsNesInputState nes;
  HsNintendo64InputState nintendo_64;
  HsNintendoDsInputState nintendo_ds;
  HsPcEngineInputState pc_engine;
  HsPlayStationInputState psx;
  HsSegaSaturnInputState saturn;
  HsSg1000InputState sg1000;
  HsSuperNesInputState super_nes;
  HsVirtualBoyInputState virtual_boy;
  HsWonderSwanInputState wonderswan;
};

typedef union _HsInputState HsInputState;

G_END_DECLS
