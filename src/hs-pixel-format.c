/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-pixel-format.h"

/**
 * HsPixelFormat:
 * @HS_PIXEL_FORMAT_R8G8B8: A pixel is 24 bits long, with 8 bit for red, green
 *     and blue channels.
 * @HS_PIXEL_FORMAT_R8G8B8X8: A pixel is 32 bits long, with 8 unused bits, then
 *     8 bits for red, green and blue channels each.
 * @HS_PIXEL_FORMAT_B8G8R8X8: A pixel is 32 bits long, with 8 bits for blue,
 *     green and red channels each, followed by 8 unused bits.
 *
 * Represents the pixel formats to use for [iface@SoftwareContext] framebuffer.
 */

/**
 * hs_pixel_format_get_pixel_size:
 * @format: a pixel format
 *
 * Gets the size of a pixel in a given format, in bytes.
 *
 * Returns: the size of a pixel
 */
int
hs_pixel_format_get_pixel_size (HsPixelFormat format)
{
  switch (format) {
  case HS_PIXEL_FORMAT_R8G8B8:
    return 3;
  case HS_PIXEL_FORMAT_R8G8B8X8:
  case HS_PIXEL_FORMAT_B8G8R8X8:
    return 4;
  default:
    g_assert_not_reached ();
  }
}
