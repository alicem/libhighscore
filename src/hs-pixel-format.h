/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-enums.h"

G_BEGIN_DECLS

typedef enum {
  HS_PIXEL_FORMAT_R8G8B8,
  HS_PIXEL_FORMAT_R8G8B8X8,
  HS_PIXEL_FORMAT_B8G8R8X8,
} HsPixelFormat;

int hs_pixel_format_get_pixel_size (HsPixelFormat format);

G_END_DECLS
