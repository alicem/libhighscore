/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-platform.h"

/**
 * HsPlatform:
 * @HS_PLATFORM_UNKNOWN: An unset value.
 * @HS_PLATFORM_ATARI_2600: Atari 2600.
 * @HS_PLATFORM_ATARI_7800: Atari 7800.
 * @HS_PLATFORM_ATARI_LYNX: Atari Lynx.
 * @HS_PLATFORM_FAMICOM_DISK_SYSTEM: Famicom Disk System, an add-on for
 *     [enum@Hs.Platform.NES].
 * @HS_PLATFORM_GAME_BOY: Game Boy and Game Boy Color.
 * @HS_PLATFORM_GAME_BOY_ADVANCE: Game Boy Advance.
 * @HS_PLATFORM_GAME_GEAR: Game Gear.
 * @HS_PLATFORM_MASTER_SYSTEM: Sega Master System.
 * @HS_PLATFORM_NEO_GEO_POCKET: Neo Geo Pocket and Neo Geo Pocket Color.
 * @HS_PLATFORM_NES: Nintendo Entertainment System / Famicom.
 * @HS_PLATFORM_NINTENDO_64: Nintendo 64.
 * @HS_PLATFORM_NINTENDO_DS: Nintendo DS.
 * @HS_PLATFORM_PC_ENGINE: PC Engine / TurboGrafx-16.
 * @HS_PLATFORM_PC_ENGINE_CD: Super CD-ROM² / TurboGrafx-CD, an add-on for
 *     [enum@Hs.Platform.PC_ENGINE].
 * @HS_PLATFORM_PLAYSTATION: PlayStation.
 * @HS_PLATFORM_MEGA_DRIVE: Mega Drive / Sega Genesis.
 * @HS_PLATFORM_SEGA_SATURN: Sega Saturn.
 * @HS_PLATFORM_SG1000: SG-1000.
 * @HS_PLATFORM_SUPER_NES: Super Nintendo Entertainment System / Super Famicom.
 * @HS_PLATFORM_VIRTUAL_BOY: Virtual Boy.
 * @HS_PLATFORM_WONDERSWAN: WonderSwan and WonderSwan Color.
 *
 * Represents the supported game platforms.
 */

/**
 * hs_platform_get_base_platform:
 * @platform: a platform
 *
 * Gets the base platform for @platform.
 *
 * If @platform is an add-on for another platform, returns the other platform.
 * Specifically, it returns:
 *
 * - [enum@Hs.Platform.NES] for [enum@Hs.Platform.FAMICOM_DISK_SYSTEM];
 * - [enum@Hs.Platform.PC_ENGINE] for [enum@Hs.Platform.PC_ENGINE_CD].
 *
 * Otherwise, returns @platform.
 *
 * Returns: the base platform
 */
HsPlatform
hs_platform_get_base_platform (HsPlatform platform)
{
  g_return_val_if_fail (platform >= HS_PLATFORM_UNKNOWN, HS_PLATFORM_UNKNOWN);
  g_return_val_if_fail (platform <= HS_PLATFORM_WONDERSWAN, HS_PLATFORM_UNKNOWN);

  switch (platform) {
  case HS_PLATFORM_FAMICOM_DISK_SYSTEM:
    return HS_PLATFORM_NES;
  case HS_PLATFORM_PC_ENGINE_CD:
    return HS_PLATFORM_PC_ENGINE;
  case HS_PLATFORM_ATARI_2600:
  case HS_PLATFORM_ATARI_7800:
  case HS_PLATFORM_ATARI_LYNX:
  case HS_PLATFORM_GAME_BOY:
  case HS_PLATFORM_GAME_BOY_ADVANCE:
  case HS_PLATFORM_GAME_GEAR:
  case HS_PLATFORM_MASTER_SYSTEM:
  case HS_PLATFORM_MEGA_DRIVE:
  case HS_PLATFORM_NEO_GEO_POCKET:
  case HS_PLATFORM_NES:
  case HS_PLATFORM_NINTENDO_64:
  case HS_PLATFORM_NINTENDO_DS:
  case HS_PLATFORM_PC_ENGINE:
  case HS_PLATFORM_PLAYSTATION:
  case HS_PLATFORM_SEGA_SATURN:
  case HS_PLATFORM_SG1000:
  case HS_PLATFORM_SUPER_NES:
  case HS_PLATFORM_VIRTUAL_BOY:
  case HS_PLATFORM_WONDERSWAN:
  case HS_PLATFORM_UNKNOWN:
    return platform;
  default:
    g_assert_not_reached ();
  }
}

/**
 * hs_platform_get_name:
 * @platform: a platform
 *
 * Gets the name of @platform.
 *
 * The names are listed in the
 * [core descriptor documentation](core-descriptors.html#platforms).
 *
 * Returns: the platform name
 */
char *
hs_platform_get_name (HsPlatform platform)
{
  const char *name;

  switch (platform) {
  case HS_PLATFORM_ATARI_2600:
    name = "Atari2600";
    break;
  case HS_PLATFORM_ATARI_7800:
    name = "Atari7800";
    break;
  case HS_PLATFORM_ATARI_LYNX:
    name = "AtariLynx";
    break;
  case HS_PLATFORM_FAMICOM_DISK_SYSTEM:
    name = "FamicomDiskSystem";
    break;
  case HS_PLATFORM_GAME_BOY:
    name = "GameBoy";
    break;
  case HS_PLATFORM_GAME_BOY_ADVANCE:
    name = "GameBoyAdvance";
    break;
  case HS_PLATFORM_GAME_GEAR:
    name = "GameGear";
    break;
  case HS_PLATFORM_MASTER_SYSTEM:
    name = "MasterSystem";
    break;
  case HS_PLATFORM_MEGA_DRIVE:
    name = "MegaDrive";
    break;
  case HS_PLATFORM_NEO_GEO_POCKET:
    name = "NeoGeoPocket";
    break;
  case HS_PLATFORM_NES:
    name = "NintendoEntertainmentSystem";
    break;
  case HS_PLATFORM_NINTENDO_64:
    name = "Nintendo64";
    break;
  case HS_PLATFORM_NINTENDO_DS:
    name = "NintendoDS";
    break;
  case HS_PLATFORM_PC_ENGINE:
    name = "PcEngine";
    break;
  case HS_PLATFORM_PC_ENGINE_CD:
    name = "PcEngineCD";
    break;
  case HS_PLATFORM_PLAYSTATION:
    name = "PlayStation";
    break;
  case HS_PLATFORM_SEGA_SATURN:
    name = "SegaSaturn";
    break;
  case HS_PLATFORM_SG1000:
    name = "SG1000";
    break;
  case HS_PLATFORM_SUPER_NES:
    name = "SuperNintendoEntertainmentSystem";
    break;
  case HS_PLATFORM_VIRTUAL_BOY:
    name = "VirtualBoy";
    break;
  case HS_PLATFORM_WONDERSWAN:
    name = "WonderSwan";
    break;
  case HS_PLATFORM_UNKNOWN:
    name = "Unknown";
    break;
  default:
    g_assert_not_reached ();
  }

  return g_strdup (name);
}

/**
 * hs_platform_get_from_name:
 * @name: a platform name
 *
 * Gets the platform matching @name.
 *
 * The names are the same ones as returned by [func@Platform.get_name].
 *
 * Returns: the platform for @name
 */
HsPlatform
hs_platform_get_from_name (const char *name)
{
  GQuark quark;

  g_return_val_if_fail (name != NULL, HS_PLATFORM_UNKNOWN);

  quark = g_quark_from_string (name);

  if (quark == g_quark_from_string ("Atari2600"))
    return HS_PLATFORM_ATARI_2600;

  if (quark == g_quark_from_string ("Atari7800"))
    return HS_PLATFORM_ATARI_7800;

  if (quark == g_quark_from_string ("AtariLynx"))
    return HS_PLATFORM_ATARI_LYNX;

  if (quark == g_quark_from_string ("FamicomDiskSystem"))
    return HS_PLATFORM_FAMICOM_DISK_SYSTEM;

  if (quark == g_quark_from_string ("GameBoy"))
    return HS_PLATFORM_GAME_BOY;

  if (quark == g_quark_from_string ("GameBoyAdvance"))
    return HS_PLATFORM_GAME_BOY_ADVANCE;

  if (quark == g_quark_from_string ("GameGear"))
    return HS_PLATFORM_GAME_GEAR;

  if (quark == g_quark_from_string ("MasterSystem"))
    return HS_PLATFORM_MASTER_SYSTEM;

  if (quark == g_quark_from_string ("MegaDrive"))
    return HS_PLATFORM_MEGA_DRIVE;

  if (quark == g_quark_from_string ("NeoGeoPocket"))
    return HS_PLATFORM_NEO_GEO_POCKET;

  if (quark == g_quark_from_string ("NintendoEntertainmentSystem"))
    return HS_PLATFORM_NES;

  if (quark == g_quark_from_string ("Nintendo64"))
    return HS_PLATFORM_NINTENDO_64;

  if (quark == g_quark_from_string ("NintendoDS"))
    return HS_PLATFORM_NINTENDO_DS;

  if (quark == g_quark_from_string ("PcEngine"))
    return HS_PLATFORM_PC_ENGINE;

  if (quark == g_quark_from_string ("PcEngineCD"))
    return HS_PLATFORM_PC_ENGINE_CD;

  if (quark == g_quark_from_string ("PlayStation"))
    return HS_PLATFORM_PLAYSTATION;

  if (quark == g_quark_from_string ("SegaSaturn"))
    return HS_PLATFORM_SEGA_SATURN;

  if (quark == g_quark_from_string ("SG1000"))
    return HS_PLATFORM_SG1000;

  if (quark == g_quark_from_string ("SuperNintendoEntertainmentSystem"))
    return HS_PLATFORM_SUPER_NES;

  if (quark == g_quark_from_string ("VirtualBoy"))
    return HS_PLATFORM_VIRTUAL_BOY;

  if (quark == g_quark_from_string ("WonderSwan"))
    return HS_PLATFORM_WONDERSWAN;

  g_critical ("Unknown platform: %s", name);

  return HS_PLATFORM_UNKNOWN;
}
