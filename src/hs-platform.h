/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib.h>

#include "hs-enums.h"

G_BEGIN_DECLS

typedef enum {
  HS_PLATFORM_UNKNOWN,
  HS_PLATFORM_ATARI_2600,
  HS_PLATFORM_ATARI_7800,
  HS_PLATFORM_ATARI_LYNX,
  HS_PLATFORM_FAMICOM_DISK_SYSTEM,
  HS_PLATFORM_GAME_BOY,
  HS_PLATFORM_GAME_BOY_ADVANCE,
  HS_PLATFORM_GAME_GEAR,
  HS_PLATFORM_MASTER_SYSTEM,
  HS_PLATFORM_MEGA_DRIVE,
  HS_PLATFORM_NEO_GEO_POCKET,
  HS_PLATFORM_NES,
  HS_PLATFORM_NINTENDO_64,
  HS_PLATFORM_NINTENDO_DS,
  HS_PLATFORM_PC_ENGINE,
  HS_PLATFORM_PC_ENGINE_CD,
  HS_PLATFORM_PLAYSTATION,
  HS_PLATFORM_SEGA_SATURN,
  HS_PLATFORM_SG1000,
  HS_PLATFORM_SUPER_NES,
  HS_PLATFORM_VIRTUAL_BOY,
  HS_PLATFORM_WONDERSWAN,
} HsPlatform;

HsPlatform hs_platform_get_base_platform (HsPlatform platform);

char *hs_platform_get_name (HsPlatform platform);

HsPlatform hs_platform_get_from_name (const char *name);

G_END_DECLS
