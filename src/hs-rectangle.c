/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-rectangle.h"

G_DEFINE_BOXED_TYPE (HsRectangle, hs_rectangle, hs_rectangle_copy, hs_rectangle_free)

/**
 * hs_rectangle_init:
 * @self: a rectangle
 * @x: the X coordinate of the top left corner
 * @y: the Y coordinate of the top left corner
 * @width: the width of the rectangle
 * @height: the height of the rectangle
 *
 * Initializes the given rectangle with the given values.
 */
void
hs_rectangle_init (HsRectangle *self,
                   int          x,
                   int          y,
                   int          width,
                   int          height)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (width >= 0);
  g_return_if_fail (height >= 0);

  self->x = x;
  self->y = y;
  self->width = width;
  self->height = height;
}

/**
 * hs_rectangle_copy:
 * @self: a rectangle
 *
 * Copies @self.
 *
 * Returns: (transfer full): a copy of @self
 */
HsRectangle *
hs_rectangle_copy (const HsRectangle *self)
{
  HsRectangle *copy;

  g_return_val_if_fail (self != NULL, NULL);

  copy = g_new0 (HsRectangle, 1);
  memcpy (copy, self, sizeof (HsRectangle));

  return copy;
}

/**
 * hs_rectangle_free:
 * @self: a rectangle
 *
 * Frees @self.
 */
void
hs_rectangle_free (HsRectangle *self)
{
  g_return_if_fail (self != NULL);

  g_free (self);
}
