/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

G_BEGIN_DECLS

/**
 * HS_RECTANGLE_INIT:
 * @_x: the X coordinate of the top left corner
 * @_y: the Y coordinate of the top left corner
 * @_width: the width of the rectangle
 * @_height: the height of the rectangle
 *
 * Initializes a [struct@Rectangle] when declaring it.
 */
#define HS_RECTANGLE_INIT(_x,_y,_width,_height) \
  (HsRectangle) { .x = (_x), .y = (_y), .width = (_width), .height = (_height) }

/**
 * HsRectangle:
 * @x: the X coordinate of the top left corner.
 * @y: the Y coordinate of the top left corner.
 * @width: the width of the rectangle.
 * @height: the height of the rectangle.
 *
 * An integer rectangle.
 *
 * Used for [method@SoftwareContext.set_area].
 */
typedef struct {
  int x;
  int y;
  int width;
  int height;
} HsRectangle;

#define HS_TYPE_RECTANGLE (hs_rectangle_get_type ())

GType hs_rectangle_get_type (void) G_GNUC_CONST;

void hs_rectangle_init (HsRectangle *self,
                        int          x,
                        int          y,
                        int          width,
                        int          height);

HsRectangle *hs_rectangle_copy (const HsRectangle *self);
void hs_rectangle_free (HsRectangle *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (HsRectangle, hs_rectangle_free)

G_END_DECLS
