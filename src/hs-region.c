/*
 * Copyright (C) 2024 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-region.h"

/**
 * HsRegion:
 * @HS_REGION_UNKNOWN: This game doesn't have a region (e.g. it's a handheld game).
 * @HS_REGION_NTSC: This game runs on NTSC TVs.
 * @HS_REGION_PAL: This game runs on PAL TVs.
 *
 * Represents game region.
 */
