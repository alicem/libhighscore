/*
 * Copyright (C) 2024 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

G_BEGIN_DECLS

typedef enum {
  HS_REGION_UNKNOWN,
  HS_REGION_NTSC,
  HS_REGION_PAL,
} HsRegion;

G_END_DECLS
