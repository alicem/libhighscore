/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-software-context.h"

G_DEFINE_INTERFACE (HsSoftwareContext, hs_software_context, G_TYPE_OBJECT)

/**
 * HsSoftwareContext:
 *
 * An interface defining an software rendering context.
 *
 * Cores should use [method@Core.create_software_context] within `load_rom()` to
 * create a context.
 *
 * Frontends must implement [vfunc@Frontend.create_software_context] and create
 * a context within it.
 *
 * The size and format of the framebuffer must be provided right away and cannot
 * be changed later, though the core is allowed to recreate the context later.
 * If the core can use multiple resolutions and it's not known at the creation
 * time, provide the maximum size, and then use a smaller area via
 * [method@SoftwareContext.set_area] and/or
 * [method@SoftwareContext.set_row_stride].
 *
 * To output the data, call [method@SoftwareContext.get_framebuffer] and write
 * your pixel data into that buffer. This must be done in
 * [vfunc@Core.run_frame].
 */

static void
hs_software_context_default_init (HsSoftwareContextInterface *iface)
{
}

/**
 * hs_software_context_set_area:
 * @self: a software context
 * @area: the new area
 *
 * Sets the usable area within the framebuffer.
 *
 * The frontend will display this area and ignore the rest of the buffer.
 */
void
hs_software_context_set_area (HsSoftwareContext *self,
                              HsRectangle       *area)
{
  HsSoftwareContextInterface *iface;

  g_return_if_fail (HS_IS_SOFTWARE_CONTEXT (self));
  g_return_if_fail (area != NULL);

  iface = HS_SOFTWARE_CONTEXT_GET_IFACE (self);

  g_assert (iface->set_area);

  return iface->set_area (self, area);
}

/**
 * hs_software_context_set_row_stride:
 * @self: a software context
 * @row_stride: the new row stride
 *
 * Sets the row stride for @self.
 *
 * By default the row stride is equal to width multiplied by the size of a
 * single pixel.
 */
void
hs_software_context_set_row_stride (HsSoftwareContext *self,
                                    guint              row_stride)
{
  HsSoftwareContextInterface *iface;

  g_return_if_fail (HS_IS_SOFTWARE_CONTEXT (self));

  iface = HS_SOFTWARE_CONTEXT_GET_IFACE (self);

  g_assert (iface->set_row_stride);

  return iface->set_row_stride (self, row_stride);
}

/**
 * hs_software_context_get_framebuffer:
 * @self: a software context
 *
 * Retrieves the framebuffer to render into.
 *
 * Returns: (transfer none): location of the framebuffer
 */
gpointer
hs_software_context_get_framebuffer (HsSoftwareContext *self)
{
  HsSoftwareContextInterface *iface;

  g_return_val_if_fail (HS_IS_SOFTWARE_CONTEXT (self), NULL);

  iface = HS_SOFTWARE_CONTEXT_GET_IFACE (self);

  g_assert (iface->get_framebuffer);

  return iface->get_framebuffer (self);
}
