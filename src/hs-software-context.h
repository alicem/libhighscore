/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-rectangle.h"

G_BEGIN_DECLS

#define HS_TYPE_SOFTWARE_CONTEXT (hs_software_context_get_type ())

G_DECLARE_INTERFACE (HsSoftwareContext, hs_software_context, HS, SOFTWARE_CONTEXT, GObject)

struct _HsSoftwareContextInterface
{
  GTypeInterface parent;

  void (* set_area) (HsSoftwareContext *self,
                     HsRectangle       *area);

  void (* set_row_stride) (HsSoftwareContext *self,
                           guint              row_stride);

  gpointer (* get_framebuffer) (HsSoftwareContext *self);
};

void hs_software_context_set_area (HsSoftwareContext *self,
                                   HsRectangle       *area);

void hs_software_context_set_row_stride (HsSoftwareContext *self,
                                         guint              row_stride);

gpointer hs_software_context_get_framebuffer (HsSoftwareContext *self);

G_END_DECLS
