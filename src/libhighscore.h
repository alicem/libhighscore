/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */
#pragma once

#include <glib.h>

G_BEGIN_DECLS

#define HS_INSIDE

#include "hs-version.h"

#include "hs-atari-7800-core.h"
#include "hs-atari-2600-core.h"
#include "hs-atari-lynx-core.h"
#include "hs-fds-core.h"
#include "hs-game-boy-core.h"
#include "hs-game-boy-advance-core.h"
#include "hs-game-gear-core.h"
#include "hs-master-system-core.h"
#include "hs-mega-drive-core.h"
#include "hs-neo-geo-pocket-core.h"
#include "hs-nes-core.h"
#include "hs-nintendo-64-core.h"
#include "hs-nintendo-ds-core.h"
#include "hs-pc-engine-core.h"
#include "hs-pc-engine-cd-core.h"
#include "hs-playstation-core.h"
#include "hs-sega-saturn-core.h"
#include "hs-sg1000-core.h"
#include "hs-super-nes-core.h"
#include "hs-virtual-boy-core.h"
#include "hs-wonderswan-core.h"

#include "hs-core.h"
#include "hs-core-error.h"
#include "hs-frontend.h"
#include "hs-gl-context.h"
#include "hs-input-state.h"
#include "hs-pixel-format.h"
#include "hs-platform.h"
#include "hs-rectangle.h"
#include "hs-region.h"
#include "hs-software-context.h"

#undef HS_INSIDE

G_END_DECLS
