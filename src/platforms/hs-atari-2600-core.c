/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-atari-2600-core.h"

/**
 * HsAtari2600Controller:
 * @HS_ATARI_2600_CONTROLLER_NONE: No controller.
 * @HS_ATARI_2600_CONTROLLER_JOYSTICK: Atari 2600 joystick.
 * @HS_ATARI_2600_CONTROLLER_GENESIS: Sega Genesis controller.
 * @HS_ATARI_2600_CONTROLLER_THREE_BUTTON: Joystick with three fire buttons.
 * @HS_ATARI_2600_CONTROLLER_DRIVING: Driving controller.
 * @HS_ATARI_2600_CONTROLLER_PADDLES: A pair of paddle controllers.
 *
 * Supported Atari 2600 controllers.
 */

/**
 * HsAtari2600TVType:
 * @HS_ATARI_2600_TV_TYPE_COLOR: Color TV.
 * @HS_ATARI_2600_TV_TYPE_BLACK_WHITE: Black & white (B-W) TV.
 *
 * Atari 2600 TV Type switch positions.
 */

/**
 * HsAtari2600Difficulty:
 * @HS_ATARI_2600_DIFFICULTY_ADVANCED: Advanced (A) difficulty.
 * @HS_ATARI_2600_DIFFICULTY_BEGINNER: Beginner (B) difficulty.
 *
 * Atari 2600 difficulty switch positions.
 */

/**
 * HsAtari2600Core:
 *
 * An interface for Atari 2600 cores.
 *
 * Defines functions specific to Atari 2600.
 */

G_DEFINE_INTERFACE (HsAtari2600Core, hs_atari_2600_core, HS_TYPE_CORE)

static void
hs_atari_2600_core_default_init (HsAtari2600CoreInterface *iface)
{
}

/**
 * hs_atari_2600_core_get_controller:
 * @self: a core
 * @player: the player
 *
 * Gets the current controller for @player.
 *
 * Returns: the controller
 */
HsAtari2600Controller
hs_atari_2600_core_get_controller (HsAtari2600Core *self,
                                   guint            player)
{
  HsAtari2600CoreInterface *iface;

  g_return_val_if_fail (HS_IS_ATARI_2600_CORE (self), HS_ATARI_2600_CONTROLLER_NONE);
  g_return_val_if_fail (player < HS_ATARI_2600_MAX_PLAYERS, HS_ATARI_2600_CONTROLLER_NONE);

  iface = HS_ATARI_2600_CORE_GET_IFACE (self);

  g_assert (iface->get_controller);

  return iface->get_controller (self, player);
}

/**
 * hs_atari_2600_core_get_default_difficulty:
 * @self: a core
 * @player: the player
 *
 * Gets the default difficulty for @player.
 *
 * Player 0 corresponds to the left difficulty switch, player 2 to the right
 * difficulty switch.
 *
 * Returns: the controller
 */
HsAtari2600Difficulty
hs_atari_2600_core_get_default_difficulty (HsAtari2600Core *self,
                                           guint            player)
{
  HsAtari2600CoreInterface *iface;

  g_return_val_if_fail (HS_IS_ATARI_2600_CORE (self), HS_ATARI_2600_DIFFICULTY_ADVANCED);
  g_return_val_if_fail (player < HS_ATARI_2600_MAX_PLAYERS, HS_ATARI_2600_DIFFICULTY_ADVANCED);

  iface = HS_ATARI_2600_CORE_GET_IFACE (self);

  g_assert (iface->get_default_difficulty);

  return iface->get_default_difficulty (self, player);
}
