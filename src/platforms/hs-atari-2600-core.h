/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-core.h"
#include "hs-enums.h"

G_BEGIN_DECLS

#define HS_TYPE_ATARI_2600_CORE (hs_atari_2600_core_get_type ())

G_DECLARE_INTERFACE (HsAtari2600Core, hs_atari_2600_core, HS, ATARI_2600_CORE, HsCore)

/**
 * HS_ATARI_2600_MAX_PLAYERS:
 *
 * The maximum number of players in Atari 2600 games.
 */
#define HS_ATARI_2600_MAX_PLAYERS 2

typedef enum {
  HS_ATARI_2600_CONTROLLER_NONE,
  HS_ATARI_2600_CONTROLLER_JOYSTICK,
  HS_ATARI_2600_CONTROLLER_GENESIS,
  HS_ATARI_2600_CONTROLLER_THREE_BUTTON,
  HS_ATARI_2600_CONTROLLER_DRIVING,
  HS_ATARI_2600_CONTROLLER_PADDLES,
} HsAtari2600Controller;

typedef enum {
  HS_ATARI_2600_JOYSTICK_BUTTON_UP,
  HS_ATARI_2600_JOYSTICK_BUTTON_DOWN,
  HS_ATARI_2600_JOYSTICK_BUTTON_LEFT,
  HS_ATARI_2600_JOYSTICK_BUTTON_RIGHT,
  HS_ATARI_2600_JOYSTICK_BUTTON_FIRE,
  HS_ATARI_2600_JOYSTICK_BUTTON_FIRE_5,
  HS_ATARI_2600_JOYSTICK_BUTTON_FIRE_9,
} HsAtari2600JoystickButton;

typedef enum {
  HS_ATARI_2600_TV_TYPE_COLOR,
  HS_ATARI_2600_TV_TYPE_BLACK_WHITE,
} HsAtari2600TVType;

typedef enum {
  HS_ATARI_2600_DIFFICULTY_ADVANCED,
  HS_ATARI_2600_DIFFICULTY_BEGINNER,
} HsAtari2600Difficulty;

typedef struct {
  guint32 joystick[HS_ATARI_2600_MAX_PLAYERS];

  double driving_axis[HS_ATARI_2600_MAX_PLAYERS];
  gboolean driving_fire[HS_ATARI_2600_MAX_PLAYERS];

  double paddle_axis[HS_ATARI_2600_MAX_PLAYERS * 2];
  double paddle_speed[HS_ATARI_2600_MAX_PLAYERS * 2];
  gboolean paddle_fire[HS_ATARI_2600_MAX_PLAYERS * 2];

  HsAtari2600TVType tv_type;
  HsAtari2600Difficulty difficulty[HS_ATARI_2600_MAX_PLAYERS];
  gboolean select_switch;
} HsAtari2600InputState;

struct _HsAtari2600CoreInterface
{
  GTypeInterface parent;

  HsAtari2600Controller (* get_controller) (HsAtari2600Core *self,
                                            guint            player);

  HsAtari2600Difficulty (* get_default_difficulty) (HsAtari2600Core *self,
                                                    guint            player);
};

HsAtari2600Controller hs_atari_2600_core_get_controller (HsAtari2600Core *self,
                                                         guint            player);

HsAtari2600Difficulty hs_atari_2600_core_get_default_difficulty (HsAtari2600Core *self,
                                                                 guint            player);

G_END_DECLS
