/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-atari-7800-core.h"

/**
 * HsAtari7800Controller:
 * @HS_ATARI_7800_CONTROLLER_NONE: No controller.
 * @HS_ATARI_7800_CONTROLLER_JOYSTICK: Atari 7800 joystick.
 * @HS_ATARI_7800_CONTROLLER_LIGHTGUN: Atari 7800 light gun.
 *
 * Supported Atari 7800 controllers.
 */

/**
 * HsAtari7800Difficulty:
 * @HS_ATARI_7800_DIFFICULTY_ADVANCED: Advanced (A) difficulty.
 * @HS_ATARI_7800_DIFFICULTY_BEGINNER: Beginner (B) difficulty.
 *
 * Atari 7800 difficulty switch positions.
 */

/**
 * HsAtari7800Core:
 *
 * An interface for Atari 7800 cores.
 *
 * Defines functions specific to Atari 7800.
 */

G_DEFINE_INTERFACE (HsAtari7800Core, hs_atari_7800_core, HS_TYPE_CORE)

static void
hs_atari_7800_core_default_init (HsAtari7800CoreInterface *iface)
{
}

/**
 * hs_atari_7800_core_get_controller:
 * @self: a core
 * @player: the player
 *
 * Gets the current controller for @player.
 *
 * Returns: the controller
 */
HsAtari7800Controller
hs_atari_7800_core_get_controller (HsAtari7800Core *self,
                                   guint            player)
{
  HsAtari7800CoreInterface *iface;

  g_return_val_if_fail (HS_IS_ATARI_7800_CORE (self), HS_ATARI_7800_CONTROLLER_NONE);
  g_return_val_if_fail (player < HS_ATARI_7800_MAX_PLAYERS, HS_ATARI_7800_CONTROLLER_NONE);

  iface = HS_ATARI_7800_CORE_GET_IFACE (self);

  g_assert (iface->get_controller);

  return iface->get_controller (self, player);
}

/**
 * hs_atari_7800_core_get_default_difficulty:
 * @self: a core
 * @player: the player
 *
 * Gets the default difficulty for @player.
 *
 * Player 0 corresponds to the left difficulty switch, player 2 to the right
 * difficulty switch.
 *
 * Returns: the controller
 */
HsAtari7800Difficulty
hs_atari_7800_core_get_default_difficulty (HsAtari7800Core *self,
                                           guint            player)
{
  HsAtari7800CoreInterface *iface;

  g_return_val_if_fail (HS_IS_ATARI_7800_CORE (self), HS_ATARI_7800_DIFFICULTY_ADVANCED);
  g_return_val_if_fail (player < HS_ATARI_7800_MAX_PLAYERS, HS_ATARI_7800_DIFFICULTY_ADVANCED);

  iface = HS_ATARI_7800_CORE_GET_IFACE (self);

  g_assert (iface->get_default_difficulty);

  return iface->get_default_difficulty (self, player);
}
