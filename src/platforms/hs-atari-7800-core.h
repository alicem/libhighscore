/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-core.h"
#include "hs-enums.h"

G_BEGIN_DECLS

#define HS_TYPE_ATARI_7800_CORE (hs_atari_7800_core_get_type ())

G_DECLARE_INTERFACE (HsAtari7800Core, hs_atari_7800_core, HS, ATARI_7800_CORE, HsCore)

/**
 * HS_ATARI_7800_MAX_PLAYERS:
 *
 * The maximum number of players in Atari 7800 games.
 */
#define HS_ATARI_7800_MAX_PLAYERS 2

typedef enum {
  HS_ATARI_7800_CONTROLLER_NONE,
  HS_ATARI_7800_CONTROLLER_JOYSTICK,
  HS_ATARI_7800_CONTROLLER_LIGHTGUN,
} HsAtari7800Controller;

typedef enum {
  HS_ATARI_7800_JOYSTICK_BUTTON_UP,
  HS_ATARI_7800_JOYSTICK_BUTTON_DOWN,
  HS_ATARI_7800_JOYSTICK_BUTTON_LEFT,
  HS_ATARI_7800_JOYSTICK_BUTTON_RIGHT,
  HS_ATARI_7800_JOYSTICK_BUTTON_ONE,
  HS_ATARI_7800_JOYSTICK_BUTTON_TWO,
} HsAtari7800JoystickButton;

typedef enum {
  HS_ATARI_7800_DIFFICULTY_ADVANCED,
  HS_ATARI_7800_DIFFICULTY_BEGINNER,
} HsAtari7800Difficulty;

typedef struct {
  guint32 joystick[HS_ATARI_7800_MAX_PLAYERS];

  gboolean lightgun_fire;
  double lightgun_x;
  double lightgun_y;

  HsAtari7800Difficulty difficulty[HS_ATARI_7800_MAX_PLAYERS];
  gboolean pause_button;
  gboolean select_button;
} HsAtari7800InputState;

struct _HsAtari7800CoreInterface
{
  GTypeInterface parent;

  HsAtari7800Controller (* get_controller) (HsAtari7800Core *self,
                                            guint            player);

  HsAtari7800Difficulty (* get_default_difficulty) (HsAtari7800Core *self,
                                                    guint            player);
};

HsAtari7800Controller hs_atari_7800_core_get_controller (HsAtari7800Core *self,
                                                         guint            player);

HsAtari7800Difficulty hs_atari_7800_core_get_default_difficulty (HsAtari7800Core *self,
                                                                 guint            player);

G_END_DECLS
