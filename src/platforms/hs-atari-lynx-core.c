/*
 * Copyright (C) 2024 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-atari-lynx-core.h"

/**
 * HsAtariLynxCore:
 *
 * An interface for Atari Lynx cores.
 *
 * Defines functions specific to Atari Lynx.
 */

G_DEFINE_INTERFACE (HsAtariLynxCore, hs_atari_lynx_core, HS_TYPE_CORE)

static void
hs_atari_lynx_core_default_init (HsAtariLynxCoreInterface *iface)
{
}

/**
 * hs_atari_lynx_core_set_bios_path:
 * @self: a core
 * @path: the path to the BIOS
 *
 * Sets the path to the Lynx boot ROM.
 */
void
hs_atari_lynx_core_set_bios_path (HsAtariLynxCore *self,
                                  const char      *path)
{
  HsAtariLynxCoreInterface *iface;

  g_return_if_fail (HS_IS_ATARI_LYNX_CORE (self));
  g_return_if_fail (path != NULL);

  iface = HS_ATARI_LYNX_CORE_GET_IFACE (self);

  g_assert (iface->set_bios_path);

  iface->set_bios_path (self, path);
}
