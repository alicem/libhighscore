/*
 * Copyright (C) 2024 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-core.h"
#include "hs-enums.h"

G_BEGIN_DECLS

#define HS_TYPE_ATARI_LYNX_CORE (hs_atari_lynx_core_get_type ())

G_DECLARE_INTERFACE (HsAtariLynxCore, hs_atari_lynx_core, HS, ATARI_LYNX_CORE, HsCore)

typedef enum {
  HS_ATARI_LYNX_BUTTON_UP,
  HS_ATARI_LYNX_BUTTON_DOWN,
  HS_ATARI_LYNX_BUTTON_LEFT,
  HS_ATARI_LYNX_BUTTON_RIGHT,
  HS_ATARI_LYNX_BUTTON_A,
  HS_ATARI_LYNX_BUTTON_B,
  HS_ATARI_LYNX_BUTTON_OPTION1,
  HS_ATARI_LYNX_BUTTON_OPTION2,
  HS_ATARI_LYNX_BUTTON_PAUSE,
} HsAtariLynxButton;

/**
 * HS_ATARI_LYNX_N_BUTTONS:
 *
 * The number of Atari Lynx buttons.
 */
#define HS_ATARI_LYNX_N_BUTTONS (HS_ATARI_LYNX_BUTTON_PAUSE + 1)

typedef struct {
  guint32 buttons;
} HsAtariLynxInputState;

struct _HsAtariLynxCoreInterface
{
  GTypeInterface parent;

  void (* set_bios_path) (HsAtariLynxCore *self,
                          const char      *path);
};

void hs_atari_lynx_core_set_bios_path (HsAtariLynxCore *self,
                                       const char      *path);

G_END_DECLS
