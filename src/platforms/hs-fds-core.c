/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-fds-core.h"

#include "hs-nes-core.h"

/**
 * HsFdsCore:
 *
 * An interface for Famicom Disk System cores.
 *
 * Defines functions specific to Famicom Disk System.
 *
 * Classes implementing this interface must also implement [iface@NesCore].
 */

G_DEFINE_INTERFACE_WITH_CODE (HsFdsCore, hs_fds_core, HS_TYPE_CORE,
                              g_type_interface_add_prerequisite (g_define_type_id, HS_TYPE_NES_CORE))

static void
hs_fds_core_default_init (HsFdsCoreInterface *iface)
{
}

/**
 * hs_fds_core_set_bios_path:
 * @self: a core
 * @path: the path to the BIOS
 *
 * Sets the path to the Famicom Disk System BIOS.
 */
void
hs_fds_core_set_bios_path (HsFdsCore  *self,
                           const char *path)
{
  HsFdsCoreInterface *iface;

  g_return_if_fail (HS_IS_FDS_CORE (self));
  g_return_if_fail (path != NULL);

  iface = HS_FDS_CORE_GET_IFACE (self);

  g_assert (iface->set_bios_path);

  iface->set_bios_path (self, path);
}

/**
 * hs_fds_core_get_n_sides:
 * @self: a core
 *
 * Gets the number of disk sides.
 *
 * For multi-disk games, the number of sides may go above 2. For example, if a
 * game consists of 3 disks, the number of sides will be 6.
 *
 * Returns: the number of disk sides
 */
guint
hs_fds_core_get_n_sides (HsFdsCore *self)
{
  HsFdsCoreInterface *iface;

  g_return_val_if_fail (HS_IS_FDS_CORE (self), 0);

  iface = HS_FDS_CORE_GET_IFACE (self);

  g_assert (iface->get_n_sides);

  return iface->get_n_sides (self);
}

/**
 * hs_fds_core_get_side:
 * @self: a core
 *
 * Gets the current disk side.
 *
 * Side A corresponds to 0, side B to 1.
 *
 * If a game consists of multiple disks, disk 2 side A corresponds to 3, and so
 * on.
 *
 * Returns: the current side
 */
guint
hs_fds_core_get_side (HsFdsCore *self)
{
  HsFdsCoreInterface *iface;

  g_return_val_if_fail (HS_IS_FDS_CORE (self), 0);

  iface = HS_FDS_CORE_GET_IFACE (self);

  g_assert (iface->get_side);

  return iface->get_side (self);
}

/**
 * hs_fds_core_set_side:
 * @self: a core
 * @side: the new disk side
 *
 * Sets the current disk side.
 *
 * Side A corresponds to 0, side B to 1.
 *
 * If a game consists of multiple disks, disk 2 side A corresponds to 3, and so
 * on.
 */
void
hs_fds_core_set_side (HsFdsCore *self,
                      guint      side)
{
  HsFdsCoreInterface *iface;

  g_return_if_fail (HS_IS_FDS_CORE (self));
  g_return_if_fail (side < hs_fds_core_get_n_sides (self));

  if (side == hs_fds_core_get_side (self))
    return;

  iface = HS_FDS_CORE_GET_IFACE (self);

  g_assert (iface->set_side);

  iface->set_side (self, side);
}
