/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-core.h"

G_BEGIN_DECLS

#define HS_TYPE_FDS_CORE (hs_fds_core_get_type ())

G_DECLARE_INTERFACE (HsFdsCore, hs_fds_core, HS, FDS_CORE, HsCore)

struct _HsFdsCoreInterface
{
  GTypeInterface parent;

  void (* set_bios_path) (HsFdsCore  *self,
                          const char *path);

  guint (* get_n_sides) (HsFdsCore *self);

  guint (* get_side) (HsFdsCore *self);
  void  (* set_side) (HsFdsCore *self,
                      guint      side);
};

void hs_fds_core_set_bios_path (HsFdsCore  *self,
                                const char *path);

guint hs_fds_core_get_n_sides (HsFdsCore *self);

guint hs_fds_core_get_side (HsFdsCore *self);
void  hs_fds_core_set_side (HsFdsCore *self,
                            guint      side);

G_END_DECLS
