/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-game-boy-advance-core.h"

/**
 * HsGameBoyAdvanceCore:
 *
 * An interface for Game Boy Advance cores.
 *
 * Defines functions specific to Game Boy Advance.
 */

G_DEFINE_INTERFACE (HsGameBoyAdvanceCore, hs_game_boy_advance_core, HS_TYPE_CORE)

static void
hs_game_boy_advance_core_default_init (HsGameBoyAdvanceCoreInterface *iface)
{
}
