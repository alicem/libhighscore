/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-core.h"
#include "hs-enums.h"

G_BEGIN_DECLS

#define HS_TYPE_GAME_BOY_ADVANCE_CORE (hs_game_boy_advance_core_get_type ())

G_DECLARE_INTERFACE (HsGameBoyAdvanceCore, hs_game_boy_advance_core, HS, GAME_BOY_ADVANCE_CORE, HsCore)

typedef enum {
  HS_GAME_BOY_ADVANCE_BUTTON_UP,
  HS_GAME_BOY_ADVANCE_BUTTON_DOWN,
  HS_GAME_BOY_ADVANCE_BUTTON_LEFT,
  HS_GAME_BOY_ADVANCE_BUTTON_RIGHT,
  HS_GAME_BOY_ADVANCE_BUTTON_A,
  HS_GAME_BOY_ADVANCE_BUTTON_B,
  HS_GAME_BOY_ADVANCE_BUTTON_SELECT,
  HS_GAME_BOY_ADVANCE_BUTTON_START,
  HS_GAME_BOY_ADVANCE_BUTTON_L,
  HS_GAME_BOY_ADVANCE_BUTTON_R,
} HsGameBoyAdvanceButton;

/**
 * HS_GAME_BOY_ADVANCE_N_BUTTONS:
 *
 * The number of Game Boy Advance buttons.
 */
#define HS_GAME_BOY_ADVANCE_N_BUTTONS (HS_GAME_BOY_ADVANCE_BUTTON_R + 1)

typedef struct {
  guint32 buttons;
} HsGameBoyAdvanceInputState;

struct _HsGameBoyAdvanceCoreInterface
{
  GTypeInterface parent;
};

G_END_DECLS
