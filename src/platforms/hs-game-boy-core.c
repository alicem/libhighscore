/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-game-boy-core.h"

G_DEFINE_INTERFACE (HsGameBoyCore, hs_game_boy_core, HS_TYPE_CORE)

/**
 * HsGameBoyModel:
 * @HS_GAME_BOY_MODEL_GAME_BOY: Game Boy (DMG).
 * @HS_GAME_BOY_MODEL_GAME_BOY_POCKET: Game Boy Pocket.
 * @HS_GAME_BOY_MODEL_GAME_BOY_COLOR: Game Boy Color.
 * @HS_GAME_BOY_MODEL_GAME_BOY_ADVANCE: Game Boy Advance.
 * @HS_GAME_BOY_MODEL_SUPER_GAME_BOY: Super Game Boy.
 * @HS_GAME_BOY_MODEL_SUPER_GAME_BOY_2: Super Game Boy 2.
 *
 * Game Boy models.
 */

/**
 * HsGameBoyCore:
 *
 * An interface for Game Boy and Game Boy Color cores.
 *
 * Defines functions specific to Game Boy and Game Boy Color.
 */

static void
hs_game_boy_core_default_init (HsGameBoyCoreInterface *iface)
{
}

/**
 * hs_game_boy_core_set_model:
 * @self: a core
 * @model: the new model
 *
 * Sets the Game Boy model.
 */
void
hs_game_boy_core_set_model (HsGameBoyCore  *self,
                            HsGameBoyModel  model)
{
  HsGameBoyCoreInterface *iface;

  g_return_if_fail (HS_IS_GAME_BOY_CORE (self));
  g_return_if_fail (model >= HS_GAME_BOY_MODEL_GAME_BOY);
  g_return_if_fail (model <= HS_GAME_BOY_MODEL_SUPER_GAME_BOY_2);

  iface = HS_GAME_BOY_CORE_GET_IFACE (self);

  g_assert (iface->set_model);

  iface->set_model (self, model);
}

/**
 * hs_game_boy_core_set_palette:
 * @self: a core
 * @colors: (array length=n_colors): colors
 * @n_colors: length of @colors
 *
 * Sets a palette to use for Game Boy games.
 *
 * @n_colors must be either 4 or 12.
 *
 * Each of the colors in @colors is an sRGB color in the hex representation.
 *
 * Does not affect Game Boy Color and Super Game Boy games.
 */
void
hs_game_boy_core_set_palette (HsGameBoyCore *self,
                              int           *colors,
                              int            n_colors)
{
  HsGameBoyCoreInterface *iface;

  g_return_if_fail (HS_IS_GAME_BOY_CORE (self));
  g_return_if_fail (colors != NULL);
  g_return_if_fail (n_colors == 4 || n_colors == 12);

  iface = HS_GAME_BOY_CORE_GET_IFACE (self);

  g_assert (iface->set_palette);

  iface->set_palette (self, colors, n_colors);
}
