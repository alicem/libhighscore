/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-core.h"
#include "hs-enums.h"

G_BEGIN_DECLS

#define HS_TYPE_GAME_BOY_CORE (hs_game_boy_core_get_type ())

G_DECLARE_INTERFACE (HsGameBoyCore, hs_game_boy_core, HS, GAME_BOY_CORE, HsCore)

typedef enum {
  HS_GAME_BOY_BUTTON_UP,
  HS_GAME_BOY_BUTTON_DOWN,
  HS_GAME_BOY_BUTTON_LEFT,
  HS_GAME_BOY_BUTTON_RIGHT,
  HS_GAME_BOY_BUTTON_A,
  HS_GAME_BOY_BUTTON_B,
  HS_GAME_BOY_BUTTON_SELECT,
  HS_GAME_BOY_BUTTON_START,
} HsGameBoyButton;

/**
 * HS_GAME_BOY_N_BUTTONS:
 *
 * The number of Game Boy buttons.
 */
#define HS_GAME_BOY_N_BUTTONS (HS_GAME_BOY_BUTTON_START + 1)

typedef struct {
  guint32 buttons;
} HsGameBoyInputState;

typedef enum {
  HS_GAME_BOY_MODEL_GAME_BOY,
  HS_GAME_BOY_MODEL_GAME_BOY_POCKET,
  HS_GAME_BOY_MODEL_GAME_BOY_COLOR,
  HS_GAME_BOY_MODEL_GAME_BOY_ADVANCE,
  HS_GAME_BOY_MODEL_SUPER_GAME_BOY,
  HS_GAME_BOY_MODEL_SUPER_GAME_BOY_2,
} HsGameBoyModel;

struct _HsGameBoyCoreInterface
{
  GTypeInterface parent;

  void (* set_model) (HsGameBoyCore  *self,
                      HsGameBoyModel  model);

  void (* set_palette) (HsGameBoyCore *self,
                        int           *colors,
                        int            n_colors);
};

void hs_game_boy_core_set_model (HsGameBoyCore  *self,
                                 HsGameBoyModel  model);

void hs_game_boy_core_set_palette (HsGameBoyCore *self,
                                   int           *colors,
                                   int            n_colors);

G_END_DECLS
