/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-game-gear-core.h"

/**
 * HsGameGearCore:
 *
 * An interface for Game Gear cores.
 *
 * Defines functions specific to Game Gear.
 */

G_DEFINE_INTERFACE (HsGameGearCore, hs_game_gear_core, HS_TYPE_CORE)

static void
hs_game_gear_core_default_init (HsGameGearCoreInterface *iface)
{
}
