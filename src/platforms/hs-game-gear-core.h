/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-core.h"
#include "hs-enums.h"

G_BEGIN_DECLS

#define HS_TYPE_GAME_GEAR_CORE (hs_game_gear_core_get_type ())

G_DECLARE_INTERFACE (HsGameGearCore, hs_game_gear_core, HS, GAME_GEAR_CORE, HsCore)

typedef enum {
  HS_GAME_GEAR_BUTTON_UP,
  HS_GAME_GEAR_BUTTON_DOWN,
  HS_GAME_GEAR_BUTTON_LEFT,
  HS_GAME_GEAR_BUTTON_RIGHT,
  HS_GAME_GEAR_BUTTON_ONE,
  HS_GAME_GEAR_BUTTON_TWO,
  HS_GAME_GEAR_BUTTON_START,
} HsGameGearButton;

/**
 * HS_GAME_GEAR_N_BUTTONS:
 *
 * The number of Game Gear buttons.
 */
#define HS_GAME_GEAR_N_BUTTONS (HS_GAME_GEAR_BUTTON_START + 1)

typedef struct {
  guint32 buttons;
} HsGameGearInputState;

struct _HsGameGearCoreInterface
{
  GTypeInterface parent;
};

G_END_DECLS
