/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-master-system-core.h"

/**
 * HsMasterSystemCore:
 *
 * An interface for Master System cores.
 *
 * Defines functions specific to Master System.
 */

G_DEFINE_INTERFACE (HsMasterSystemCore, hs_master_system_core, HS_TYPE_CORE)

static guint
hs_master_system_core_real_get_players (HsMasterSystemCore *self)
{
  return HS_MASTER_SYSTEM_MAX_PLAYERS;
}

static void
hs_master_system_core_default_init (HsMasterSystemCoreInterface *iface)
{
  iface->get_players = hs_master_system_core_real_get_players;
}

/**
 * hs_master_system_core_get_players:
 * @self: a core
 *
 * Gets the number of players in the current game.
 *
 * If not overridden, returns [const@MASTER_SYSTEM_MAX_PLAYERS].
 *
 * Returns: the number of players
 */
guint
hs_master_system_core_get_players (HsMasterSystemCore *self)
{
  HsMasterSystemCoreInterface *iface;

  g_return_val_if_fail (HS_IS_MASTER_SYSTEM_CORE (self), 0);

  iface = HS_MASTER_SYSTEM_CORE_GET_IFACE (self);

  g_assert (iface->get_players);

  return iface->get_players (self);
}

/**
 * hs_master_system_core_set_enable_fm_audio:
 * @self: a core
 * @enable_fm_audio: whether to use FM audio
 *
 * Sets whether to use FM audio.
 *
 * Only works for games that support the FM Sound Unit.
 */
void
hs_master_system_core_set_enable_fm_audio (HsMasterSystemCore *self,
                                           gboolean            enable_fm_audio)
{
  HsMasterSystemCoreInterface *iface;

  g_return_if_fail (HS_IS_MASTER_SYSTEM_CORE (self));

  iface = HS_MASTER_SYSTEM_CORE_GET_IFACE (self);

  g_assert (iface->set_enable_fm_audio);

  iface->set_enable_fm_audio (self, enable_fm_audio);
}

/**
 * hs_master_system_core_set_enable_light_phaser:
 * @self: a core
 * @enable_light_phaser: whether to use Light Phaser
 *
 * Sets whether to use Light Phaser.
 *
 * Only works for games that support Light Phaser.
 */
void
hs_master_system_core_set_enable_light_phaser (HsMasterSystemCore *self,
                                               gboolean            enable_light_phaser)
{
  HsMasterSystemCoreInterface *iface;

  g_return_if_fail (HS_IS_MASTER_SYSTEM_CORE (self));

  iface = HS_MASTER_SYSTEM_CORE_GET_IFACE (self);

  g_assert (iface->set_enable_light_phaser);

  iface->set_enable_light_phaser (self, enable_light_phaser);
}
