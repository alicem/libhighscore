/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-core.h"
#include "hs-enums.h"

G_BEGIN_DECLS

#define HS_TYPE_MASTER_SYSTEM_CORE (hs_master_system_core_get_type ())

G_DECLARE_INTERFACE (HsMasterSystemCore, hs_master_system_core, HS, MASTER_SYSTEM_CORE, HsCore)

typedef enum {
  HS_MASTER_SYSTEM_BUTTON_UP,
  HS_MASTER_SYSTEM_BUTTON_DOWN,
  HS_MASTER_SYSTEM_BUTTON_LEFT,
  HS_MASTER_SYSTEM_BUTTON_RIGHT,
  HS_MASTER_SYSTEM_BUTTON_ONE,
  HS_MASTER_SYSTEM_BUTTON_TWO,
} HsMasterSystemButton;

/**
 * HS_MASTER_SYSTEM_N_BUTTONS:
 *
 * The number of Master System pad buttons.
 */
#define HS_MASTER_SYSTEM_N_BUTTONS (HS_MASTER_SYSTEM_BUTTON_TWO + 1)

/**
 * HS_MASTER_SYSTEM_MAX_PLAYERS:
 *
 * The maximum number of players in Master System games.
 */
#define HS_MASTER_SYSTEM_MAX_PLAYERS 2

typedef struct {
  guint32 pad_buttons[HS_MASTER_SYSTEM_MAX_PLAYERS];
  gboolean pause_button;

  gboolean light_phaser_fire;
  double light_phaser_x;
  double light_phaser_y;
} HsMasterSystemInputState;

struct _HsMasterSystemCoreInterface
{
  GTypeInterface parent;

  guint (* get_players) (HsMasterSystemCore *self);

  void (*set_enable_fm_audio) (HsMasterSystemCore *self,
                               gboolean            enable_fm_audio);

  void (*set_enable_light_phaser) (HsMasterSystemCore *self,
                                   gboolean            enable_light_phaser);
};

guint hs_master_system_core_get_players (HsMasterSystemCore *self);

void hs_master_system_core_set_enable_fm_audio (HsMasterSystemCore *self,
                                                gboolean            enable_fm_audio);

void hs_master_system_core_set_enable_light_phaser (HsMasterSystemCore *self,
                                                    gboolean            enable_light_phaser);

G_END_DECLS
