/*
 * Copyright (C) 2024 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-mega-drive-core.h"

/**
 * HsMegaDriveCore:
 *
 * An interface for Mega Drive cores.
 *
 * Defines functions specific to Mega Drive.
 */

G_DEFINE_INTERFACE (HsMegaDriveCore, hs_mega_drive_core, HS_TYPE_CORE)

static guint
hs_mega_drive_core_real_get_players (HsMegaDriveCore *self)
{
  return HS_MEGA_DRIVE_MAX_PLAYERS;
}

static void
hs_mega_drive_core_default_init (HsMegaDriveCoreInterface *iface)
{
  iface->get_players = hs_mega_drive_core_real_get_players;
}

/**
 * hs_mega_drive_core_get_players:
 * @self: a core
 *
 * Gets the number of players in the current game.
 *
 * If not overridden, returns [const@MEGA_DRIVE_MAX_PLAYERS].
 *
 * Returns: the number of players
 */
guint
hs_mega_drive_core_get_players (HsMegaDriveCore *self)
{
  HsMegaDriveCoreInterface *iface;

  g_return_val_if_fail (HS_IS_MEGA_DRIVE_CORE (self), 0);

  iface = HS_MEGA_DRIVE_CORE_GET_IFACE (self);

  g_assert (iface->get_players);

  return iface->get_players (self);
}
