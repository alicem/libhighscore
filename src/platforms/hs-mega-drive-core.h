/*
 * Copyright (C) 2024 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-core.h"
#include "hs-enums.h"

G_BEGIN_DECLS

#define HS_TYPE_MEGA_DRIVE_CORE (hs_mega_drive_core_get_type ())

G_DECLARE_INTERFACE (HsMegaDriveCore, hs_mega_drive_core, HS, MEGA_DRIVE_CORE, HsCore)

typedef enum {
  HS_MEGA_DRIVE_BUTTON_UP,
  HS_MEGA_DRIVE_BUTTON_DOWN,
  HS_MEGA_DRIVE_BUTTON_LEFT,
  HS_MEGA_DRIVE_BUTTON_RIGHT,
  HS_MEGA_DRIVE_BUTTON_A,
  HS_MEGA_DRIVE_BUTTON_B,
  HS_MEGA_DRIVE_BUTTON_C,
  HS_MEGA_DRIVE_BUTTON_X,
  HS_MEGA_DRIVE_BUTTON_Y,
  HS_MEGA_DRIVE_BUTTON_Z,
  HS_MEGA_DRIVE_BUTTON_START,
  HS_MEGA_DRIVE_BUTTON_MODE,
} HsMegaDriveButton;

/**
 * HS_MEGA_DRIVE_N_BUTTONS:
 *
 * The number of Mega Drive pad buttons.
 */
#define HS_MEGA_DRIVE_N_BUTTONS (HS_MEGA_DRIVE_BUTTON_MODE + 1)

/**
 * HS_MEGA_DRIVE_MAX_PLAYERS:
 *
 * The maximum number of players in Mega Drive games.
 */
#define HS_MEGA_DRIVE_MAX_PLAYERS 2

typedef struct {
  guint32 pad_buttons[HS_MEGA_DRIVE_MAX_PLAYERS];
} HsMegaDriveInputState;

struct _HsMegaDriveCoreInterface
{
  GTypeInterface parent;

  guint (* get_players) (HsMegaDriveCore *self);
};

guint hs_mega_drive_core_get_players (HsMegaDriveCore *self);

G_END_DECLS
