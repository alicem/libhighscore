/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-neo-geo-pocket-core.h"

/**
 * HsNeoGeoPocketCore:
 *
 * An interface for Neo Geo Pocket and Neo Geo Pocket Color cores.
 *
 * Defines functions specific to Neo Geo Pocket and Neo Geo Pocket Color.
 */

G_DEFINE_INTERFACE (HsNeoGeoPocketCore, hs_neo_geo_pocket_core, HS_TYPE_CORE)

static void
hs_neo_geo_pocket_core_default_init (HsNeoGeoPocketCoreInterface *iface)
{
}
