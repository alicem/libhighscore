/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-core.h"
#include "hs-enums.h"

G_BEGIN_DECLS

#define HS_TYPE_NEO_GEO_POCKET_CORE (hs_neo_geo_pocket_core_get_type ())

G_DECLARE_INTERFACE (HsNeoGeoPocketCore, hs_neo_geo_pocket_core, HS, NEO_GEO_POCKET_CORE, HsCore)

typedef enum {
  HS_NEO_GEO_POCKET_BUTTON_UP,
  HS_NEO_GEO_POCKET_BUTTON_DOWN,
  HS_NEO_GEO_POCKET_BUTTON_LEFT,
  HS_NEO_GEO_POCKET_BUTTON_RIGHT,
  HS_NEO_GEO_POCKET_BUTTON_A,
  HS_NEO_GEO_POCKET_BUTTON_B,
  HS_NEO_GEO_POCKET_BUTTON_OPTION,
} HsNeoGeoPocketButton;

/**
 * HS_NEO_GEO_POCKET_N_BUTTONS:
 *
 * The number of Neo Geo Pocket buttons.
 */
#define HS_NEO_GEO_POCKET_N_BUTTONS (HS_NEO_GEO_POCKET_BUTTON_OPTION + 1)

typedef struct {
  guint32 buttons;
} HsNeoGeoPocketInputState;

struct _HsNeoGeoPocketCoreInterface
{
  GTypeInterface parent;
};

G_END_DECLS
