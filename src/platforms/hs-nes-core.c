/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-nes-core.h"

/**
 * HsNesMicLevel:
 * @HS_NES_MIC_LEVEL_QUIET: Quiet.
 * @HS_NES_MIC_LEVEL_LOUD: Loud.
 *
 * Famicom mic level.
 */

/**
 * HsNesAccessory:
 * @HS_NES_ACCESSORY_NONE: No accessory.
 * @HS_NES_ACCESSORY_ZAPPER: NES Zapper.
 * @HS_NES_ACCESSORY_PADDLE: Arkanoid Vaus controller.
 *
 * Supported NES accessories.
 */

/**
 * HsNesCore:
 *
 * An interface for NES cores.
 *
 * Defines functions specific to NES.
 */

G_DEFINE_INTERFACE (HsNesCore, hs_nes_core, HS_TYPE_CORE)

static guint
hs_nes_core_real_get_players (HsNesCore *self)
{
  return HS_NES_MAX_PLAYERS;
}

static void
hs_nes_core_default_init (HsNesCoreInterface *iface)
{
  iface->get_players = hs_nes_core_real_get_players;
}

/**
 * hs_nes_core_get_players:
 * @self: a core
 *
 * Gets the number of players in the current game.
 *
 * If not overridden, returns [const@NES_MAX_PLAYERS].
 *
 * Returns: the number of players
 */
guint
hs_nes_core_get_players (HsNesCore *self)
{
  HsNesCoreInterface *iface;

  g_return_val_if_fail (HS_IS_NES_CORE (self), 0);

  iface = HS_NES_CORE_GET_IFACE (self);

  g_assert (iface->get_players);

  return iface->get_players (self);
}

/**
 * hs_nes_core_get_has_mic:
 * @self: a core
 *
 * Gets whether the current game supports mic input.
 *
 * Returns: whether the current game supports mic
 */
gboolean
hs_nes_core_get_has_mic (HsNesCore *self)
{
  HsNesCoreInterface *iface;

  g_return_val_if_fail (HS_IS_NES_CORE (self), FALSE);

  iface = HS_NES_CORE_GET_IFACE (self);

  g_assert (iface->get_has_mic);

  return iface->get_has_mic (self);
}

/**
 * hs_nes_core_get_accessory:
 * @self: a core
 *
 * Gets the current accessory, if any.
 *
 * Returns: the current accessory, or `HS_NES_ACCESSORY_NONE`
 */
HsNesAccessory
hs_nes_core_get_accessory (HsNesCore *self)
{
  HsNesCoreInterface *iface;

  g_return_val_if_fail (HS_IS_NES_CORE (self), HS_NES_ACCESSORY_NONE);

  iface = HS_NES_CORE_GET_IFACE (self);

  g_assert (iface->get_accessory);

  return iface->get_accessory (self);
}
