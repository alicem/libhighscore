/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-core.h"
#include "hs-enums.h"

G_BEGIN_DECLS

#define HS_TYPE_NES_CORE (hs_nes_core_get_type ())

G_DECLARE_INTERFACE (HsNesCore, hs_nes_core, HS, NES_CORE, HsCore)

typedef enum {
  HS_NES_BUTTON_UP,
  HS_NES_BUTTON_DOWN,
  HS_NES_BUTTON_LEFT,
  HS_NES_BUTTON_RIGHT,
  HS_NES_BUTTON_A,
  HS_NES_BUTTON_B,
  HS_NES_BUTTON_SELECT,
  HS_NES_BUTTON_START,
} HsNesButton;

/**
 * HS_NES_N_BUTTONS:
 *
 * The number of NES pad buttons.
 */
#define HS_NES_N_BUTTONS (HS_NES_BUTTON_START + 1)

/**
 * HS_NES_MAX_PLAYERS:
 *
 * The maximum number of players in NES games.
 */
#define HS_NES_MAX_PLAYERS 4

typedef enum {
  HS_NES_MIC_LEVEL_QUIET,
  HS_NES_MIC_LEVEL_LOUD,
} HsNesMicLevel;

typedef enum {
  HS_NES_ACCESSORY_NONE,
  HS_NES_ACCESSORY_ZAPPER,
  HS_NES_ACCESSORY_PADDLE,
} HsNesAccessory;

typedef struct {
  guint32 pad_buttons[HS_NES_MAX_PLAYERS];
  HsNesMicLevel mic_level;

  gboolean zapper_fire;
  double zapper_x;
  double zapper_y;

  double paddle_position;
  double paddle_speed;
  gboolean paddle_button;
} HsNesInputState;

struct _HsNesCoreInterface
{
  GTypeInterface parent;

  guint (* get_players) (HsNesCore *self);

  gboolean (* get_has_mic) (HsNesCore *self);

  HsNesAccessory (* get_accessory) (HsNesCore *self);
};

guint hs_nes_core_get_players (HsNesCore *self);

gboolean hs_nes_core_get_has_mic (HsNesCore *self);

HsNesAccessory hs_nes_core_get_accessory (HsNesCore *self);

G_END_DECLS
