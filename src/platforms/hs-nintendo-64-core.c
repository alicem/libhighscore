/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-nintendo-64-core.h"

/**
 * HsNintendo64Pak:
 * @HS_NINTENDO_64_PAK_NONE: No expansion.
 * @HS_NINTENDO_64_PAK_MEMORY_PAK: Controller Pak / Memory Pak, required for
 *     saving in some games.
 * @HS_NINTENDO_64_PAK_RUMBLE_PAK: Rumble Pak, provides rumble support.
 *
 * Nintendo 64 controller expansions.
 */

/**
 * HsNintendo64Core:
 *
 * An interface for Nintendo 64 cores.
 *
 * Defines functions specific to Nintendo 64.
 */

G_DEFINE_INTERFACE (HsNintendo64Core, hs_nintendo_64_core, HS_TYPE_CORE)

static void
hs_nintendo_64_core_default_init (HsNintendo64CoreInterface *iface)
{
}

/**
 * hs_nintendo_64_core_get_players:
 * @self: a core
 *
 * Gets the number of players in the current game.
 *
 * Returns: the number of players
 */
guint
hs_nintendo_64_core_get_players (HsNintendo64Core *self)
{
  HsNintendo64CoreInterface *iface;

  g_return_val_if_fail (HS_IS_NINTENDO_64_CORE (self), 0);

  iface = HS_NINTENDO_64_CORE_GET_IFACE (self);

  g_assert (iface->get_players);

  return iface->get_players (self);
}

/**
 * hs_nintendo_64_core_set_controller:
 * @self: a core
 * @player: the player
 * @present: whether a controller is plugged in
 * @pak: the extension plugged into the controller
 *
 * Sets the controller for @player.
 */
void
hs_nintendo_64_core_set_controller (HsNintendo64Core *self,
                                    guint             player,
                                    gboolean          present,
                                    HsNintendo64Pak   pak)
{
  HsNintendo64CoreInterface *iface;

  g_return_if_fail (HS_IS_NINTENDO_64_CORE (self));
  g_return_if_fail (player < HS_NINTENDO_64_MAX_PLAYERS);
  g_return_if_fail (pak >= HS_NINTENDO_64_PAK_NONE);
  g_return_if_fail (pak <= HS_NINTENDO_64_PAK_RUMBLE_PAK);

  iface = HS_NINTENDO_64_CORE_GET_IFACE (self);

  g_assert (iface->set_controller);

  iface->set_controller (self, player, present, pak);
}
