/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-core.h"
#include "hs-enums.h"

G_BEGIN_DECLS

#define HS_TYPE_NINTENDO_64_CORE (hs_nintendo_64_core_get_type ())

G_DECLARE_INTERFACE (HsNintendo64Core, hs_nintendo_64_core, HS, NINTENDO_64_CORE, HsCore)

typedef enum {
  HS_NINTENDO_64_BUTTON_UP,
  HS_NINTENDO_64_BUTTON_DOWN,
  HS_NINTENDO_64_BUTTON_LEFT,
  HS_NINTENDO_64_BUTTON_RIGHT,
  HS_NINTENDO_64_BUTTON_A,
  HS_NINTENDO_64_BUTTON_B,
  HS_NINTENDO_64_BUTTON_C_UP,
  HS_NINTENDO_64_BUTTON_C_DOWN,
  HS_NINTENDO_64_BUTTON_C_LEFT,
  HS_NINTENDO_64_BUTTON_C_RIGHT,
  HS_NINTENDO_64_BUTTON_L,
  HS_NINTENDO_64_BUTTON_R,
  HS_NINTENDO_64_BUTTON_Z,
  HS_NINTENDO_64_BUTTON_START,
} HsNintendo64Button;

/**
 * HS_NINTENDO_64_N_BUTTONS:
 *
 * The number of Nintendo 64 pad buttons.
 */
#define HS_NINTENDO_64_N_BUTTONS (HS_NINTENDO_64_BUTTON_START + 1)

/**
 * HS_NINTENDO_64_MAX_PLAYERS:
 *
 * The maximum number of players in Nintendo 64 games.
 */
#define HS_NINTENDO_64_MAX_PLAYERS 4

typedef struct {
  guint32 pad_buttons[HS_NINTENDO_64_MAX_PLAYERS];
  double pad_control_stick_x[HS_NINTENDO_64_MAX_PLAYERS];
  double pad_control_stick_y[HS_NINTENDO_64_MAX_PLAYERS];
} HsNintendo64InputState;

typedef enum {
  HS_NINTENDO_64_PAK_NONE,
  HS_NINTENDO_64_PAK_MEMORY_PAK,
  HS_NINTENDO_64_PAK_RUMBLE_PAK,
} HsNintendo64Pak;

struct _HsNintendo64CoreInterface
{
  GTypeInterface parent;

  guint (* get_players) (HsNintendo64Core *self);

  void (* set_controller) (HsNintendo64Core *self,
                           guint             player,
                           gboolean          present,
                           HsNintendo64Pak   pak);
};

guint hs_nintendo_64_core_get_players (HsNintendo64Core *self);

void hs_nintendo_64_core_set_controller (HsNintendo64Core *self,
                                         guint             player,
                                         gboolean          present,
                                         HsNintendo64Pak   pak);

G_END_DECLS
