/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-nintendo-ds-core.h"

/**
 * HsNintendoDsCore:
 *
 * An interface for Nintendo DS cores.
 *
 * Defines functions specific to Nintendo DS.
 *
 * Nintendo DS cores are expected to output the two screens stacked vertically,
 * with no gap. The frontend will split the screens into whatever layout it
 * needs.
 */

G_DEFINE_INTERFACE (HsNintendoDsCore, hs_nintendo_ds_core, HS_TYPE_CORE)

static void
hs_nintendo_ds_core_default_init (HsNintendoDsCoreInterface *iface)
{
}
