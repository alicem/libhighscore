/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-core.h"
#include "hs-enums.h"

G_BEGIN_DECLS

#define HS_TYPE_NINTENDO_DS_CORE (hs_nintendo_ds_core_get_type ())

G_DECLARE_INTERFACE (HsNintendoDsCore, hs_nintendo_ds_core, HS, NINTENDO_DS_CORE, HsCore)

typedef enum {
  HS_NINTENDO_DS_BUTTON_UP,
  HS_NINTENDO_DS_BUTTON_DOWN,
  HS_NINTENDO_DS_BUTTON_LEFT,
  HS_NINTENDO_DS_BUTTON_RIGHT,
  HS_NINTENDO_DS_BUTTON_A,
  HS_NINTENDO_DS_BUTTON_B,
  HS_NINTENDO_DS_BUTTON_X,
  HS_NINTENDO_DS_BUTTON_Y,
  HS_NINTENDO_DS_BUTTON_SELECT,
  HS_NINTENDO_DS_BUTTON_START,
  HS_NINTENDO_DS_BUTTON_L,
  HS_NINTENDO_DS_BUTTON_R,
} HsNintendoDsButton;

/**
 * HS_NINTENDO_DS_N_BUTTONS:
 *
 * The number of Nintendo DS buttons.
 */
#define HS_NINTENDO_DS_N_BUTTONS (HS_NINTENDO_DS_BUTTON_R + 1)

typedef struct {
  guint32 buttons;
  gboolean touch_pressed;
  double touch_x;
  double touch_y;
  gboolean mic_active;
} HsNintendoDsInputState;

struct _HsNintendoDsCoreInterface
{
  GTypeInterface parent;
};

G_END_DECLS
