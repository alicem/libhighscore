/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-pc-engine-cd-core.h"

#include "hs-pc-engine-core.h"

/**
 * HsPcEngineCdCore:
 *
 * An interface for TurboGrafx-CD cores.
 *
 * Defines functions specific to TurboGrafx-CD.
 *
 * Classes implementing this interface must also implement [iface@PcEngineCore].
 */

G_DEFINE_INTERFACE_WITH_CODE (HsPcEngineCdCore, hs_pc_engine_cd_core, HS_TYPE_CORE,
                              g_type_interface_add_prerequisite (g_define_type_id, HS_TYPE_PC_ENGINE_CORE))

static void
hs_pc_engine_cd_core_default_init (HsPcEngineCdCoreInterface *iface)
{
}

/**
 * hs_pc_engine_cd_core_set_bios_path:
 * @self: a core
 * @path: the path to the BIOS
 *
 * Sets the path to the System Card 3.
 */
void
hs_pc_engine_cd_core_set_bios_path (HsPcEngineCdCore *self,
                                    const char       *path)
{
  HsPcEngineCdCoreInterface *iface;

  g_return_if_fail (HS_IS_PC_ENGINE_CD_CORE (self));
  g_return_if_fail (path != NULL);

  iface = HS_PC_ENGINE_CD_CORE_GET_IFACE (self);

  g_assert (iface->set_bios_path);

  iface->set_bios_path (self, path);
}
