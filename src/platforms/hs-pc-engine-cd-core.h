/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-core.h"
#include "hs-enums.h"

G_BEGIN_DECLS

#define HS_TYPE_PC_ENGINE_CD_CORE (hs_pc_engine_cd_core_get_type ())

G_DECLARE_INTERFACE (HsPcEngineCdCore, hs_pc_engine_cd_core, HS, PC_ENGINE_CD_CORE, HsCore)

struct _HsPcEngineCdCoreInterface
{
  GTypeInterface parent;

  void (* set_bios_path) (HsPcEngineCdCore  *self,
                          const char      *path);
};

void hs_pc_engine_cd_core_set_bios_path (HsPcEngineCdCore *self,
                                         const char       *path);

G_END_DECLS
