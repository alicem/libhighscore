/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-pc-engine-core.h"

/**
 * HsPcEnginePadMode:
 * @HS_PC_ENGINE_TWO_BUTTONS: Use two face buttons.
 * @HS_PC_ENGINE_SIX_BUTTONS: Use six face buttons.
 *
 * TurboGrafx-16 pad mode.
 *
 * Most games only work with 2-button mode, but some have improved controls for
 * 6-button mode.
 */

/**
 * HsPcEngineCore:
 *
 * An interface for TurboGrafx-16 cores.
 *
 * Defines functions specific to TurboGrafx-16.
 */

G_DEFINE_INTERFACE (HsPcEngineCore, hs_pc_engine_core, HS_TYPE_CORE)

static guint
hs_pc_engine_core_real_get_players (HsPcEngineCore *self)
{
  return HS_PC_ENGINE_MAX_PLAYERS;
}

static void
hs_pc_engine_core_default_init (HsPcEngineCoreInterface *iface)
{
  iface->get_players = hs_pc_engine_core_real_get_players;
}

/**
 * hs_pc_engine_core_get_players:
 * @self: a core
 *
 * Gets the number of players in the current game.
 *
 * If not overridden, returns [const@PC_ENGINE_MAX_PLAYERS].
 *
 * Returns: the number of players
 */
guint
hs_pc_engine_core_get_players (HsPcEngineCore *self)
{
  HsPcEngineCoreInterface *iface;

  g_return_val_if_fail (HS_IS_PC_ENGINE_CORE (self), 0);

  iface = HS_PC_ENGINE_CORE_GET_IFACE (self);

  g_assert (iface->get_players);

  return iface->get_players (self);
}
