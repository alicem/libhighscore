/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-core.h"
#include "hs-enums.h"

G_BEGIN_DECLS

#define HS_TYPE_PC_ENGINE_CORE (hs_pc_engine_core_get_type ())

G_DECLARE_INTERFACE (HsPcEngineCore, hs_pc_engine_core, HS, PC_ENGINE_CORE, HsCore)

typedef enum {
  HS_PC_ENGINE_BUTTON_UP,
  HS_PC_ENGINE_BUTTON_DOWN,
  HS_PC_ENGINE_BUTTON_LEFT,
  HS_PC_ENGINE_BUTTON_RIGHT,
  HS_PC_ENGINE_BUTTON_I,
  HS_PC_ENGINE_BUTTON_II,
  HS_PC_ENGINE_BUTTON_III,
  HS_PC_ENGINE_BUTTON_IV,
  HS_PC_ENGINE_BUTTON_V,
  HS_PC_ENGINE_BUTTON_VI,
  HS_PC_ENGINE_BUTTON_SELECT,
  HS_PC_ENGINE_BUTTON_RUN,
} HsPcEngineButton;

/**
 * HS_PC_ENGINE_N_BUTTONS:
 *
 * The number of TurboGrafx-16 pad buttons.
 */
#define HS_PC_ENGINE_N_BUTTONS (HS_PC_ENGINE_BUTTON_RUN + 1)

/**
 * HS_PC_ENGINE_MAX_PLAYERS:
 *
 * The maximum number of players in TurboGrafx-16 games.
 */
#define HS_PC_ENGINE_MAX_PLAYERS 5

typedef enum {
  HS_PC_ENGINE_TWO_BUTTONS,
  HS_PC_ENGINE_SIX_BUTTONS,
} HsPcEnginePadMode;

typedef struct {
  guint32 pad_buttons[HS_PC_ENGINE_MAX_PLAYERS];
  HsPcEnginePadMode pad_mode[HS_PC_ENGINE_MAX_PLAYERS];
} HsPcEngineInputState;

struct _HsPcEngineCoreInterface
{
  GTypeInterface parent;

  guint (* get_players) (HsPcEngineCore *self);
};

guint hs_pc_engine_core_get_players (HsPcEngineCore *self);

G_END_DECLS
