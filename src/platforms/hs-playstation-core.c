/*
 * Copyright (C) 2024 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-playstation-core.h"

/**
 * HsPlayStationBios:
 * @HS_PLAYSTATION_BIOS_JP: Japanese BIOS (scph5500.bin)
 * @HS_PLAYSTATION_BIOS_US: US BIOS (scph5501.bin)
 * @HS_PLAYSTATION_BIOS_EU: European BIOS (scph5502.bin)
 *
 * BIOS type for [method@PlayStationCore.set_bios_path].
 */

/**
 * HsPlayStationDualShockMode:
 * @HS_PLAYSTATION_DUALSHOCK_DIGITAL: Digital mode
 * @HS_PLAYSTATION_DUALSHOCK_ANALOG: Analog mode
 *
 * DualShock modes for [method@PlayStationCore.get_dualshock_mode] and
 * [method@PlayStationCore.set_dualshock_mode].
 */

/**
 * HsPlayStationCore:
 *
 * An interface for PlayStation cores.
 *
 * Defines functions specific to PlayStation.
 */

G_DEFINE_INTERFACE (HsPlayStationCore, hs_playstation_core, HS_TYPE_CORE)

enum {
  SIGNAL_DUALSHOCK_MODE_CHANGED,
  SIGNAL_LAST_SIGNAL,
};

static guint signals[SIGNAL_LAST_SIGNAL];

static guint
hs_playstation_core_real_get_players (HsPlayStationCore *self)
{
  return HS_PLAYSTATION_MAX_PLAYERS;
}

static void
hs_playstation_core_default_init (HsPlayStationCoreInterface *iface)
{
  iface->get_players = hs_playstation_core_real_get_players;

  /**
   * HsPlayStationCore::dualshock-mode-changed:
   * @self: a core
   * @player: the player
   *
   * Emitted when DualShock mode changes for @player.
   *
   * Use [method@PlayStationCore.get_dualshock_mode] to get the new mode.
   */
  signals[SIGNAL_DUALSHOCK_MODE_CHANGED] =
    g_signal_new ("dualshock-mode-changed",
                  G_TYPE_FROM_INTERFACE (iface),
                  G_SIGNAL_RUN_FIRST,
                  0,
                  NULL, NULL, NULL,
                  G_TYPE_NONE,
                  1,
                  G_TYPE_UINT);
}

/**
 * hs_playstation_core_get_players:
 * @self: a core
 *
 * Gets the number of players in the current game.
 *
 * If not overridden, returns [const@PLAYSTATION_MAX_PLAYERS].
 *
 * Returns: the number of players
 */
guint
hs_playstation_core_get_players (HsPlayStationCore *self)
{
  HsPlayStationCoreInterface *iface;

  g_return_val_if_fail (HS_IS_PLAYSTATION_CORE (self), 0);

  iface = HS_PLAYSTATION_CORE_GET_IFACE (self);

  g_assert (iface->get_players);

  return iface->get_players (self);
}

/**
 * hs_playstation_core_get_dualshock_mode:
 * @self: a core
 * @player: the player
 *
 * Gets the mode DualShock is using for @player.
 *
 * If not using DualShock, returns [enum@Hs.PlayStationDualShockMode.DIGITAL].
 *
 * Returns: the DualShock mode
 */
HsPlayStationDualShockMode
hs_playstation_core_get_dualshock_mode (HsPlayStationCore *self,
                                        guint              player)
{
  HsPlayStationCoreInterface *iface;

  g_return_val_if_fail (HS_IS_PLAYSTATION_CORE (self), HS_PLAYSTATION_DUALSHOCK_DIGITAL);
  g_return_val_if_fail (player < HS_PLAYSTATION_MAX_PLAYERS, HS_PLAYSTATION_DUALSHOCK_DIGITAL);

  iface = HS_PLAYSTATION_CORE_GET_IFACE (self);

  g_assert (iface->get_dualshock_mode);

  return iface->get_dualshock_mode (self, player);
}

/**
 * hs_playstation_core_set_dualshock_mode:
 * @self: a core
 * @player: the player
 * @mode: the new mode
 *
 * Sets the mode DualShock is using for @player.
 *
 * This can fail if the game doesn't allow to change modes.
 *
 * Returns: whether the mode was changed
 */
gboolean
hs_playstation_core_set_dualshock_mode (HsPlayStationCore          *self,
                                        guint                       player,
                                        HsPlayStationDualShockMode  mode)
{
  HsPlayStationCoreInterface *iface;

  g_return_val_if_fail (HS_IS_PLAYSTATION_CORE (self), FALSE);
  g_return_val_if_fail (player < HS_PLAYSTATION_MAX_PLAYERS, FALSE);
  g_return_val_if_fail (mode >= HS_PLAYSTATION_DUALSHOCK_DIGITAL, FALSE);
  g_return_val_if_fail (mode <= HS_PLAYSTATION_DUALSHOCK_ANALOG, FALSE);

  iface = HS_PLAYSTATION_CORE_GET_IFACE (self);

  g_assert (iface->set_dualshock_mode);

  return iface->set_dualshock_mode (self, player, mode);
}

/**
 * hs_playstation_core_emit_dualshock_mode_changed: (emitter dualshock-mode-changed)
 * @self: a core
 * @player: the player
 *
 * Notifies frontend when DualShock mode changes for @player.
 *
 * See [method@PlayStationCore.set_dualshock_mode].
 */
void
hs_playstation_core_emit_dualshock_mode_changed (HsPlayStationCore *self,
                                                 guint              player)
{
  g_return_if_fail (HS_IS_PLAYSTATION_CORE (self));
  g_return_if_fail (player < HS_PLAYSTATION_MAX_PLAYERS);

  g_signal_emit (self, signals[SIGNAL_DUALSHOCK_MODE_CHANGED], 0, player);
}

/**
 * hs_playstation_core_set_bios_path:
 * @self: a core
 * @type: The BIOS type
 * @path: the path to the BIOS
 *
 * Sets the path to the PlayStation BIOS specified by @type.
 */
void
hs_playstation_core_set_bios_path (HsPlayStationCore *self,
                                   HsPlayStationBios  type,
                                   const char        *path)
{
  HsPlayStationCoreInterface *iface;

  g_return_if_fail (HS_IS_PLAYSTATION_CORE (self));
  g_return_if_fail (type <= HS_PLAYSTATION_BIOS_N_BIOS);
  g_return_if_fail (path != NULL);

  iface = HS_PLAYSTATION_CORE_GET_IFACE (self);

  g_assert (iface->set_bios_path);

  iface->set_bios_path (self, type, path);
}

/**
 * hs_playstation_core_get_used_bios:
 * @self: a core
 *
 * Gets the currently used PlayStation BIOS type.
 *
 * Returns: the BIOS type
 */
HsPlayStationBios
hs_playstation_core_get_used_bios (HsPlayStationCore *self)
{
  HsPlayStationCoreInterface *iface;

  g_return_val_if_fail (HS_IS_PLAYSTATION_CORE (self), HS_PLAYSTATION_BIOS_JP);

  iface = HS_PLAYSTATION_CORE_GET_IFACE (self);

  g_assert (iface->get_used_bios);

  return iface->get_used_bios (self);
}
