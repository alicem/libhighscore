/*
 * Copyright (C) 2024 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-core.h"
#include "hs-enums.h"

G_BEGIN_DECLS

#define HS_TYPE_PLAYSTATION_CORE (hs_playstation_core_get_type ())

G_DECLARE_INTERFACE (HsPlayStationCore, hs_playstation_core, HS, PLAYSTATION_CORE, HsCore)

typedef enum {
  HS_PLAYSTATION_BIOS_JP,
  HS_PLAYSTATION_BIOS_US,
  HS_PLAYSTATION_BIOS_EU,
  HS_PLAYSTATION_BIOS_N_BIOS,
} HsPlayStationBios;

typedef enum {
  HS_PLAYSTATION_BUTTON_UP,
  HS_PLAYSTATION_BUTTON_DOWN,
  HS_PLAYSTATION_BUTTON_LEFT,
  HS_PLAYSTATION_BUTTON_RIGHT,
  HS_PLAYSTATION_BUTTON_TRIANGLE,
  HS_PLAYSTATION_BUTTON_SQUARE,
  HS_PLAYSTATION_BUTTON_CIRCLE,
  HS_PLAYSTATION_BUTTON_CROSS,
  HS_PLAYSTATION_BUTTON_L1,
  HS_PLAYSTATION_BUTTON_L2,
  HS_PLAYSTATION_BUTTON_L3,
  HS_PLAYSTATION_BUTTON_R1,
  HS_PLAYSTATION_BUTTON_R2,
  HS_PLAYSTATION_BUTTON_R3,
  HS_PLAYSTATION_BUTTON_SELECT,
  HS_PLAYSTATION_BUTTON_START,
} HsPlayStationButton;

typedef enum {
  HS_PLAYSTATION_STICK_LEFT,
  HS_PLAYSTATION_STICK_RIGHT,
} HsPlayStationStick;

typedef enum {
  HS_PLAYSTATION_DUALSHOCK_DIGITAL,
  HS_PLAYSTATION_DUALSHOCK_ANALOG,
} HsPlayStationDualShockMode;

/**
 * HS_PLAYSTATION_N_BUTTONS:
 *
 * The number of PlayStation pad buttons.
 */
#define HS_PLAYSTATION_N_BUTTONS (HS_PLAYSTATION_BUTTON_START + 1)

/**
 * HS_PLAYSTATION_N_STICKS:
 *
 * The number of PlayStation pad sticks.
 */
#define HS_PLAYSTATION_N_STICKS (HS_PLAYSTATION_STICK_RIGHT + 1)

/**
 * HS_PLAYSTATION_MAX_PLAYERS:
 *
 * The maximum number of players in PlayStation games.
 */
#define HS_PLAYSTATION_MAX_PLAYERS 4

typedef struct {
  guint32 pad_buttons[HS_PLAYSTATION_MAX_PLAYERS];
  double pad_sticks_x[HS_PLAYSTATION_N_STICKS * HS_PLAYSTATION_MAX_PLAYERS];
  double pad_sticks_y[HS_PLAYSTATION_N_STICKS * HS_PLAYSTATION_MAX_PLAYERS];
} HsPlayStationInputState;

struct _HsPlayStationCoreInterface
{
  GTypeInterface parent;

  guint (* get_players) (HsPlayStationCore *self);

  HsPlayStationDualShockMode (* get_dualshock_mode) (HsPlayStationCore          *self,
                                                     guint                       player);
  gboolean                   (* set_dualshock_mode) (HsPlayStationCore          *self,
                                                     guint                       player,
                                                     HsPlayStationDualShockMode  mode);

  void (* set_bios_path) (HsPlayStationCore *self,
                          HsPlayStationBios  type,
                          const char        *path);

  HsPlayStationBios (* get_used_bios) (HsPlayStationCore *self);
};

guint hs_playstation_core_get_players (HsPlayStationCore *self);

HsPlayStationDualShockMode hs_playstation_core_get_dualshock_mode (HsPlayStationCore          *self,
                                                                   guint                       player);
gboolean                   hs_playstation_core_set_dualshock_mode (HsPlayStationCore          *self,
                                                                   guint                       player,
                                                                   HsPlayStationDualShockMode  mode);

void hs_playstation_core_emit_dualshock_mode_changed (HsPlayStationCore *self,
                                                      guint              player);

void hs_playstation_core_set_bios_path (HsPlayStationCore *self,
                                        HsPlayStationBios  type,
                                        const char        *path);

HsPlayStationBios hs_playstation_core_get_used_bios (HsPlayStationCore *self);

G_END_DECLS
