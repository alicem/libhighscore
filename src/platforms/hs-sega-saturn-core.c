/*
 * Copyright (C) 2024 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-sega-saturn-core.h"

/**
 * HsSegaSaturnBios:
 * @HS_SEGA_SATURN_BIOS_JP: Japanese BIOS (sega_101.bin)
 * @HS_SEGA_SATURN_BIOS_US_EU: International BIOS (mpr-17933.bin)
 *
 * BIOS type for [method@SegaSaturnCore.set_bios_path].
 */

/**
 * HsSegaSaturnController:
 * @HS_SEGA_SATURN_3D_PAD_DIGITAL: Standard Control Pad
 * @HS_SEGA_SATURN_3D_PAD_ANALOG: 3D Control Pad
 *
 * Controller type for [method@SegaSaturnCore.set_controller].
 */

/**
 * HsSegaSaturn3DPadMode:
 * @HS_SEGA_SATURN_CONTROL_PAD: Prefer digital controls
 * @HS_SEGA_SATURN_3D_CONTROL_PAD: Prefer analog controls
 *
 * Describes the controller types for the 3D Control Pad
 */

/**
 * HsSegaSaturnCore:
 *
 * An interface for Sega Saturn cores.
 *
 * Defines functions specific to Sega Saturn.
 */

G_DEFINE_INTERFACE (HsSegaSaturnCore, hs_sega_saturn_core, HS_TYPE_CORE)

static guint
hs_sega_saturn_core_real_get_players (HsSegaSaturnCore *self)
{
  return HS_SEGA_SATURN_MAX_PLAYERS;
}

static void
hs_sega_saturn_core_default_init (HsSegaSaturnCoreInterface *iface)
{
  iface->get_players = hs_sega_saturn_core_real_get_players;
}

/**
 * hs_sega_saturn_core_get_players:
 * @self: a core
 *
 * Gets the number of players in the current game.
 *
 * If not overridden, returns [const@SEGA_SATURN_MAX_PLAYERS].
 *
 * Returns: the number of players
 */
guint
hs_sega_saturn_core_get_players (HsSegaSaturnCore *self)
{
  HsSegaSaturnCoreInterface *iface;

  g_return_val_if_fail (HS_IS_SEGA_SATURN_CORE (self), 0);

  iface = HS_SEGA_SATURN_CORE_GET_IFACE (self);

  g_assert (iface->get_players);

  return iface->get_players (self);
}

/**
 * hs_sega_saturn_core_set_controller:
 * @self: a core
 * @player: the player
 * @controller: the controller to use for this player
 *
 * Sets the controller for @player.
 */
void
hs_sega_saturn_core_set_controller (HsSegaSaturnCore       *self,
                                    guint                   player,
                                    HsSegaSaturnController  controller)
{
  HsSegaSaturnCoreInterface *iface;

  g_return_if_fail (HS_IS_SEGA_SATURN_CORE (self));
  g_return_if_fail (player < HS_SEGA_SATURN_MAX_PLAYERS);
  g_return_if_fail (controller >= HS_SEGA_SATURN_CONTROL_PAD);
  g_return_if_fail (controller <= HS_SEGA_SATURN_3D_CONTROL_PAD);

  iface = HS_SEGA_SATURN_CORE_GET_IFACE (self);

  g_assert (iface->set_controller);

  iface->set_controller (self, player, controller);
}

/**
 * hs_sega_saturn_core_set_bios_path:
 * @self: a core
 * @type: The BIOS type
 * @path: the path to the BIOS
 *
 * Sets the path to the Sega Saturn BIOS specified by @type.
 */
void
hs_sega_saturn_core_set_bios_path (HsSegaSaturnCore *self,
                                   HsSegaSaturnBios  type,
                                   const char       *path)
{
  HsSegaSaturnCoreInterface *iface;

  g_return_if_fail (HS_IS_SEGA_SATURN_CORE (self));
  g_return_if_fail (type <= HS_SEGA_SATURN_BIOS_N_BIOS);
  g_return_if_fail (path != NULL);

  iface = HS_SEGA_SATURN_CORE_GET_IFACE (self);

  g_assert (iface->set_bios_path);

  iface->set_bios_path (self, type, path);
}

/**
 * hs_sega_saturn_core_get_used_bios:
 * @self: a core
 *
 * Gets the currently used Sega Saturn BIOS type.
 *
 * Returns: the BIOS type
 */
HsSegaSaturnBios
hs_sega_saturn_core_get_used_bios (HsSegaSaturnCore *self)
{
  HsSegaSaturnCoreInterface *iface;

  g_return_val_if_fail (HS_IS_SEGA_SATURN_CORE (self), HS_SEGA_SATURN_BIOS_JP);

  iface = HS_SEGA_SATURN_CORE_GET_IFACE (self);

  g_assert (iface->get_used_bios);

  return iface->get_used_bios (self);
}
