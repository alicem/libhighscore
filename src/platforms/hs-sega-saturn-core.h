/*
 * Copyright (C) 2024 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-core.h"
#include "hs-enums.h"

G_BEGIN_DECLS

#define HS_TYPE_SEGA_SATURN_CORE (hs_sega_saturn_core_get_type ())

G_DECLARE_INTERFACE (HsSegaSaturnCore, hs_sega_saturn_core, HS, SEGA_SATURN_CORE, HsCore)

typedef enum {
  HS_SEGA_SATURN_BIOS_JP,
  HS_SEGA_SATURN_BIOS_US_EU,
  HS_SEGA_SATURN_BIOS_N_BIOS /*< skip >*/
} HsSegaSaturnBios;

typedef enum {
  HS_SEGA_SATURN_BUTTON_UP,
  HS_SEGA_SATURN_BUTTON_DOWN,
  HS_SEGA_SATURN_BUTTON_LEFT,
  HS_SEGA_SATURN_BUTTON_RIGHT,
  HS_SEGA_SATURN_BUTTON_A,
  HS_SEGA_SATURN_BUTTON_B,
  HS_SEGA_SATURN_BUTTON_C,
  HS_SEGA_SATURN_BUTTON_X,
  HS_SEGA_SATURN_BUTTON_Y,
  HS_SEGA_SATURN_BUTTON_Z,
  HS_SEGA_SATURN_BUTTON_L,
  HS_SEGA_SATURN_BUTTON_R,
  HS_SEGA_SATURN_BUTTON_START,
} HsSegaSaturnButton;

typedef enum {
  HS_SEGA_SATURN_CONTROL_PAD,
  HS_SEGA_SATURN_3D_CONTROL_PAD,
} HsSegaSaturnController;

typedef enum {
  HS_SEGA_SATURN_3D_PAD_DIGITAL,
  HS_SEGA_SATURN_3D_PAD_ANALOG,
} HsSegaSaturn3DPadMode;

/**
 * HS_SEGA_SATURN_N_BUTTONS:
 *
 * The number of Sega Saturn pad buttons.
 */
#define HS_SEGA_SATURN_N_BUTTONS (HS_SEGA_SATURN_BUTTON_START + 1)

/**
 * HS_SEGA_SATURN_MAX_PLAYERS:
 *
 * The maximum number of players in Sega Saturn games.
 */
#define HS_SEGA_SATURN_MAX_PLAYERS 4

typedef struct {
  guint32 pad_buttons[HS_SEGA_SATURN_MAX_PLAYERS];

  HsSegaSaturn3DPadMode pad_mode[HS_SEGA_SATURN_MAX_PLAYERS];
  double pad_stick_x[HS_SEGA_SATURN_MAX_PLAYERS];
  double pad_stick_y[HS_SEGA_SATURN_MAX_PLAYERS];
  double pad_left_trigger[HS_SEGA_SATURN_MAX_PLAYERS];
  double pad_right_trigger[HS_SEGA_SATURN_MAX_PLAYERS];
} HsSegaSaturnInputState;

struct _HsSegaSaturnCoreInterface
{
  GTypeInterface parent;

  guint (* get_players) (HsSegaSaturnCore *self);

  void (* set_controller) (HsSegaSaturnCore       *self,
                           guint                   player,
                           HsSegaSaturnController  controller);

  void (* set_bios_path) (HsSegaSaturnCore *self,
                          HsSegaSaturnBios  type,
                          const char       *path);

  HsSegaSaturnBios (* get_used_bios) (HsSegaSaturnCore *self);
};

guint hs_sega_saturn_core_get_players (HsSegaSaturnCore *self);

void hs_sega_saturn_core_set_controller (HsSegaSaturnCore       *self,
                                         guint                   player,
                                         HsSegaSaturnController  controller);

void hs_sega_saturn_core_set_bios_path (HsSegaSaturnCore *self,
                                        HsSegaSaturnBios  type,
                                        const char       *path);

HsSegaSaturnBios hs_sega_saturn_core_get_used_bios (HsSegaSaturnCore *self);

G_END_DECLS
