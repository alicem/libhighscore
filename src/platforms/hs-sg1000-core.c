/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-sg1000-core.h"

/**
 * HsSg1000Core:
 *
 * An interface for SG-1000 cores.
 *
 * Defines functions specific to SG-1000.
 */

G_DEFINE_INTERFACE (HsSg1000Core, hs_sg1000_core, HS_TYPE_CORE)

static guint
hs_sg1000_core_real_get_players (HsSg1000Core *self)
{
  return HS_SG1000_MAX_PLAYERS;
}

static void
hs_sg1000_core_default_init (HsSg1000CoreInterface *iface)
{
  iface->get_players = hs_sg1000_core_real_get_players;
}

/**
 * hs_sg1000_core_get_players:
 * @self: a core
 *
 * Gets the number of players in the current game.
 *
 * If not overridden, returns [const@SG1000_MAX_PLAYERS].
 *
 * Returns: the number of players
 */
guint
hs_sg1000_core_get_players (HsSg1000Core *self)
{
  HsSg1000CoreInterface *iface;

  g_return_val_if_fail (HS_IS_SG1000_CORE (self), 0);

  iface = HS_SG1000_CORE_GET_IFACE (self);

  g_assert (iface->get_players);

  return iface->get_players (self);
}
