/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-core.h"
#include "hs-enums.h"

G_BEGIN_DECLS

#define HS_TYPE_SG1000_CORE (hs_sg1000_core_get_type ())

G_DECLARE_INTERFACE (HsSg1000Core, hs_sg1000_core, HS, SG1000_CORE, HsCore)

typedef enum {
  HS_SG1000_BUTTON_UP,
  HS_SG1000_BUTTON_DOWN,
  HS_SG1000_BUTTON_LEFT,
  HS_SG1000_BUTTON_RIGHT,
  HS_SG1000_BUTTON_ONE,
  HS_SG1000_BUTTON_TWO,
} HsSg1000Button;

/**
 * HS_SG1000_N_BUTTONS:
 *
 * The number of SG-1000 pad buttons.
 */
#define HS_SG1000_N_BUTTONS (HS_SG1000_BUTTON_TWO + 1)

/**
 * HS_SG1000_MAX_PLAYERS:
 *
 * The maximum number of players in SG-1000 games.
 */
#define HS_SG1000_MAX_PLAYERS 2

typedef struct {
  guint32 pad_buttons[HS_SG1000_MAX_PLAYERS];
  gboolean pause_button;
} HsSg1000InputState;

struct _HsSg1000CoreInterface
{
  GTypeInterface parent;

  guint (* get_players) (HsSg1000Core *self);
};

guint hs_sg1000_core_get_players (HsSg1000Core *self);

G_END_DECLS
