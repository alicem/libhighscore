/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-super-nes-core.h"

/**
 * HsSuperNesCore:
 *
 * An interface for Super NES cores.
 *
 * Defines functions specific to Super NES.
 */

G_DEFINE_INTERFACE (HsSuperNesCore, hs_super_nes_core, HS_TYPE_CORE)

static guint
hs_super_nes_core_real_get_players (HsSuperNesCore *self)
{
  return HS_SUPER_NES_MAX_PLAYERS;
}

static void
hs_super_nes_core_default_init (HsSuperNesCoreInterface *iface)
{
  iface->get_players = hs_super_nes_core_real_get_players;
}

/**
 * hs_super_nes_core_get_players:
 * @self: a core
 *
 * Gets the number of players in the current game.
 *
 * If not overridden, returns [const@SUPER_NES_MAX_PLAYERS].
 *
 * Returns: the number of players
 */
guint
hs_super_nes_core_get_players (HsSuperNesCore *self)
{
  HsSuperNesCoreInterface *iface;

  g_return_val_if_fail (HS_IS_SUPER_NES_CORE (self), 0);

  iface = HS_SUPER_NES_CORE_GET_IFACE (self);

  g_assert (iface->get_players);

  return iface->get_players (self);
}
