/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-core.h"
#include "hs-enums.h"

G_BEGIN_DECLS

#define HS_TYPE_SUPER_NES_CORE (hs_super_nes_core_get_type ())

G_DECLARE_INTERFACE (HsSuperNesCore, hs_super_nes_core, HS, SUPER_NES_CORE, HsCore)

typedef enum {
  HS_SUPER_NES_BUTTON_UP,
  HS_SUPER_NES_BUTTON_DOWN,
  HS_SUPER_NES_BUTTON_LEFT,
  HS_SUPER_NES_BUTTON_RIGHT,
  HS_SUPER_NES_BUTTON_A,
  HS_SUPER_NES_BUTTON_B,
  HS_SUPER_NES_BUTTON_X,
  HS_SUPER_NES_BUTTON_Y,
  HS_SUPER_NES_BUTTON_SELECT,
  HS_SUPER_NES_BUTTON_START,
  HS_SUPER_NES_BUTTON_L,
  HS_SUPER_NES_BUTTON_R,
} HsSuperNesButton;

/**
 * HS_SUPER_NES_N_BUTTONS:
 *
 * The number of Super NES pad buttons.
 */
#define HS_SUPER_NES_N_BUTTONS (HS_SUPER_NES_BUTTON_R + 1)

/**
 * HS_SUPER_NES_MAX_PLAYERS:
 *
 * The maximum number of players in Super NES games.
 */
#define HS_SUPER_NES_MAX_PLAYERS 4

typedef struct {
  guint32 pad_buttons[HS_SUPER_NES_MAX_PLAYERS];
} HsSuperNesInputState;

struct _HsSuperNesCoreInterface
{
  GTypeInterface parent;

  guint (* get_players) (HsSuperNesCore *self);
};

guint hs_super_nes_core_get_players (HsSuperNesCore *self);

G_END_DECLS
