/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-virtual-boy-core.h"

/**
 * HsVirtualBoyCore:
 *
 * An interface for Virtual Boy cores.
 *
 * Defines functions specific to Virtual Boy.
 *
 * Virtual Boy cores are expected to output the two screens together as a
 * red/blue image, with the left screen in the red channel and the right screen
 * in the blue channel. The frontend will recolor it as needed.
 */

G_DEFINE_INTERFACE (HsVirtualBoyCore, hs_virtual_boy_core, HS_TYPE_CORE)

static void
hs_virtual_boy_core_default_init (HsVirtualBoyCoreInterface *iface)
{
}
