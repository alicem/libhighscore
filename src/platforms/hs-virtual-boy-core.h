/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-core.h"
#include "hs-enums.h"

G_BEGIN_DECLS

#define HS_TYPE_VIRTUAL_BOY_CORE (hs_virtual_boy_core_get_type ())

G_DECLARE_INTERFACE (HsVirtualBoyCore, hs_virtual_boy_core, HS, VIRTUAL_BOY_CORE, HsCore)

typedef enum {
  HS_VIRTUAL_BOY_BUTTON_L_UP,
  HS_VIRTUAL_BOY_BUTTON_L_DOWN,
  HS_VIRTUAL_BOY_BUTTON_L_LEFT,
  HS_VIRTUAL_BOY_BUTTON_L_RIGHT,
  HS_VIRTUAL_BOY_BUTTON_R_UP,
  HS_VIRTUAL_BOY_BUTTON_R_DOWN,
  HS_VIRTUAL_BOY_BUTTON_R_LEFT,
  HS_VIRTUAL_BOY_BUTTON_R_RIGHT,
  HS_VIRTUAL_BOY_BUTTON_A,
  HS_VIRTUAL_BOY_BUTTON_B,
  HS_VIRTUAL_BOY_BUTTON_SELECT,
  HS_VIRTUAL_BOY_BUTTON_START,
  HS_VIRTUAL_BOY_BUTTON_L,
  HS_VIRTUAL_BOY_BUTTON_R,
} HsVirtualBoyButton;

/**
 * HS_VIRTUAL_BOY_N_BUTTONS:
 *
 * The number of Virtual Boy buttons.
 */
#define HS_VIRTUAL_BOY_N_BUTTONS (HS_VIRTUAL_BOY_BUTTON_R + 1)

typedef struct {
  guint32 buttons;
} HsVirtualBoyInputState;

struct _HsVirtualBoyCoreInterface
{
  GTypeInterface parent;
};

G_END_DECLS
