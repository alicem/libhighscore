/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "hs-wonderswan-core.h"

/**
 * HsWonderSwanCore:
 *
 * An interface for WonderSwan and WonderSwan Color cores.
 *
 * Defines functions specific to WonderSwan and WonderSwan Color.
 */

G_DEFINE_INTERFACE (HsWonderSwanCore, hs_wonderswan_core, HS_TYPE_CORE)

static void
hs_wonderswan_core_default_init (HsWonderSwanCoreInterface *iface)
{
}
