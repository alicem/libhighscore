/*
 * Copyright (C) 2023 Alice Mikhaylenko <alicem@gnome.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined(HS_INSIDE) && !defined(HS_COMPILATION)
# error "Only <libhighscore.h> can be included directly."
#endif

#include "hs-version.h"

#include <glib-object.h>

#include "hs-core.h"
#include "hs-enums.h"

G_BEGIN_DECLS

#define HS_TYPE_WONDERSWAN_CORE (hs_wonderswan_core_get_type ())

G_DECLARE_INTERFACE (HsWonderSwanCore, hs_wonderswan_core, HS, WONDERSWAN_CORE, HsCore)

typedef enum {
  HS_WONDERSWAN_BUTTON_X1,
  HS_WONDERSWAN_BUTTON_X2,
  HS_WONDERSWAN_BUTTON_X3,
  HS_WONDERSWAN_BUTTON_X4,
  HS_WONDERSWAN_BUTTON_Y1,
  HS_WONDERSWAN_BUTTON_Y2,
  HS_WONDERSWAN_BUTTON_Y3,
  HS_WONDERSWAN_BUTTON_Y4,
  HS_WONDERSWAN_BUTTON_A,
  HS_WONDERSWAN_BUTTON_B,
  HS_WONDERSWAN_BUTTON_START,
} HsWonderSwanButton;

/**
 * HS_WONDERSWAN_N_BUTTONS:
 *
 * The number of WonderSwan buttons.
 */
#define HS_WONDERSWAN_N_BUTTONS (HS_WONDERSWAN_BUTTON_START + 1)

typedef struct {
  guint32 buttons;
} HsWonderSwanInputState;

struct _HsWonderSwanCoreInterface
{
  GTypeInterface parent;
};

G_END_DECLS
